@extends('adminlte::page')
@section('title', 'Edit Adjuster Correction')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">{{ $adj_correction->field_adjuster->getFullName() }} - {{ $adj_correction->field_adjuster->state->name }}</h3>
            <a href="{{ route('adjusters.show', $adj_correction->field_adjuster->id) }}" style="float:right; color:white;">View</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.corrections', $adj_correction->field_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.log-assignments', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

        </div>
<div class="clearfix"></div>
        <form action="{{ route('adjuster-corrections.update', $adj_correction->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="box-header customHeader">
                    <h3 class="box-title"><b>Claim Number: {{ $adj_correction->claim_number}}</b></h3>
                    <a href="{{ route('adjuster-corrections.show', $adj_correction->id) }}" style="float:right; color:white;">View</a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('review_date') ? 'has-error': '' }}">
                                <label>Review Date</label>
                                <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="review_date" class="form-control datepicker" value="{{ $adj_correction->review_date }}" required="">
                                @if($errors->has('review_date'))
                                    <span class="help-block text-danger">{{ $errors->first('review_date') }}</span>
                                @endif
                            </div>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('catastrophe_claim') ? 'has-error': '' }}">
                                <label>Catastrophe Claim</label>
                                <select name="catastrophe_claim" class="form-control" required="required">
                                    <option value="" {{ ($adj_correction->catastrophe_claim == null) ? 'selected' : '' }}>--Select--</option>
                                    <option value="yes" {{ ($adj_correction->catastrophe_claim == '1') ? 'selected' : '' }}>Yes</option>
                                    <option value="no" {{ ($adj_correction->catastrophe_claim == '0') ? 'selected' : '' }}>No</option>
                                </select>
                                @if($errors->has('catastrophe_claim'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('catastrophe_claim') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('carrier') ? 'has-error': '' }}">
                                <label>Carrier</label>
                                <select name="carrier" class="form-control" required="required">
                                    <option value="">-Select--</option>
                                    @if(!empty($carriers))
                                        @foreach($carriers as $carrier)
                                            <option value="{{ $carrier->id }}" {{ ($adj_correction->carrier_id == $carrier->id) ? 'selected' : '' }}>{{ $carrier->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('carrier'))
                                    <span class="help-block text-danger">{{ $errors->first('carrier') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('claim_number') ? 'has-error': '' }}">
                                <label>Claim Number</label>
                                <input type="text" name="claim_number" class="form-control" value="{{ $adj_correction->claim_number }}" required="">
                                @if($errors->has('claim_number'))
                                    <span class="help-block text-danger">{{ $errors->first('claim_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('insd_last_name') ? 'has-error': '' }}">
                                <label>Insd Last Name</label>
                                <input type="text" name="insd_last_name" class="form-control" value="{{ $adj_correction->insd_last_name }}">
                                @if($errors->has('insd_last_name'))
                                    <span class="help-block text-danger">{{ $errors->first('insd_last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('field_adjuster') ? 'has-error': '' }}">
                                <label>Field Adjuster</label>
                                <select name="adjuster_id" class="form-control" required="required">
                                    <option value="">--Select--</option>
                                    @if(!empty($adjusters))
                                        @foreach($adjusters as $adjuster)
                                            <option value="{{ $adjuster->id }}" {{ ($adj_correction->master_adjuster_id == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('field_adjuster'))
                                    <span class="help-block text-danger">{{ $errors->first('field_adjuster') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('qa') ? 'has-error': '' }}">
                                <label>QA</label>
                                <select name="qa" class="form-control" required="required"> 
                                    <option value="">--Select--</option>
                                    @if(!empty($qas))
                                        @foreach($qas as $qa)
                                            <option value="{{ $qa->id }}" {{ ($adj_correction->qa_id == $qa->id) ? 'selected' : '' }}>{{ $qa->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('qa'))
                                    <span class="help-block text-danger">{{ $errors->first('qa') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('leader') ? 'has-error': '' }}">
                                <label>Leader</label>
                                <select name="leader" class="form-control" required="required">
                                    <option value="">--Select--</option>
                                    @if(!empty($leaders))
                                        @foreach($leaders as $leader)
                                            <option value="{{ $leader->id }}" {{ ($adj_correction->leader_id == $leader->id) ? 'selected' : '' }}>{{ $leader->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('leader'))
                                    <span class="help-block text-danger">{{ $errors->first('leader') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4><b>Field Adjuster Corrections</b></h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fa_glr_correction') ? 'has-error': '' }}">
                                        <label>FA GLR Corrections</label>
                                        <input type="number" name="fa_glr_correction" class="form-control" value="{{ $adj_correction->fa_glr_correction }}">
                                        @if($errors->has('fa_glr_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('fa_glr_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fa_est_correction') ? 'has-error': '' }}">
                                        <label>FA Est Corrections</label>
                                        <input type="number" name="fa_est_correction" class="form-control" value="{{ $adj_correction->fa_est_correction }}">
                                        @if($errors->has('fa_est_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('fa_est_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fa_photo_correction') ? 'has-error': '' }}">
                                        <label>FA Photo Corrections</label>
                                        <input type="number" name="fa_photo_correction" class="form-control" value="{{ $adj_correction->fa_photo_correction }}">
                                        @if($errors->has('fa_photo_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('fa_photo_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fa_total_correction') ? 'has-error': '' }}">
                                        <label>FA Total Corrections</label>
                                        <input type="number" name="fa_total_correction" class="form-control" value="{{ $adj_correction->fa_total_correction }}">
                                        @if($errors->has('fa_total_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('fa_total_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4><b>QA Corrections</b></h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_glr_correction') ? 'has-error': '' }}">
                                        <label>QA GLR Corrections</label>
                                        <input type="number" name="qa_glr_correction" class="form-control" value="{{ $adj_correction->qa_glr_correction }}">
                                        @if($errors->has('qa_glr_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('qa_glr_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_est_correction') ? 'has-error': '' }}">
                                        <label>QA Est Corrections</label>
                                        <input type="number" name="qa_est_correction" class="form-control" value="{{ $adj_correction->qa_est_correction }}">
                                        @if($errors->has('qa_est_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('qa_est_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_photo_correction') ? 'has-error': '' }}">
                                        <label>QA Photo Corrections</label>
                                        <input type="number" name="qa_photo_correction" class="form-control" value="{{ $adj_correction->qa_photo_correction }}">
                                        @if($errors->has('qa_photo_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('qa_photo_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_tot_correction') ? 'has-error': '' }}">
                                        <label>QA Total Corrections</label>
                                        <input type="number" name="qa_tot_correction" class="form-control" value="{{ $adj_correction->qa_total_correction }}">
                                        @if($errors->has('qa_tot_correction'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('qa_tot_correction') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('correction_details') ? 'has-error': '' }}">
                                <label>Correction Details</label>
                                <textarea class="form-control" rows="5" name="correction_details"> {{ $adj_correction->correction_detail }}</textarea>
                                @if($errors->has('correction_details'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('correction_details') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btnTheme">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
