@extends('adminlte::page')
@section('title', 'Edit User')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Create User</h3>
        </div>
        <form action="{{ route('users.update', $user->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            <label>First Name</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
                            @if($errors->has('first_name'))
                                <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            <label>Last Name</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}">
                            @if($errors->has('last_name'))
                                <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                            <label>User Name</label>
                            <input type="text" name="user_name" class="form-control" value="{{ $user->user_name }}">
                            @if($errors->has('user_name'))
                                <span class="help-block text-danger">{{ $errors->first('user_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                            @if($errors->has('email'))
                                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                            <label>Role</label>
                            <select class="form-control" name="role">
                                @if(!empty($roles))
                                        @foreach($roles as $role)
                                                <option value="{{ $role->id }}" {{ ($role->id == $user->role_id) ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                            @if($errors->has('assign-roles'))
                                <span class="help-block text-danger">{{ $errors->first('adjuster-roles') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection
