@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
<div class="box">
            <div class="box-header customHeader">
              <h3 class="box-title"><b>{{ $adjuster->getFullName() ?? "-" }} - {{ $adjuster->state->name ?? "-" }}</b></h3>
                <a href="{{ route('adjusters.edit', $adjuster->id) }}" style="float:right; color:white;">Edit</a>
            </div>

            <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            <a href="{{ route('tab.adjuster.corrections', $adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            <a href="{{ route('tab.adjuster.log-assignments', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>
            {{-- <div class="col-md-2"></div> --}}
        </div>
            <div class="clearfix"></div>
            @if($adjuster->is_flagged == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            Adjuster Flagged. See Comment
                        </div>
                    </div>
                </div>
            @endif
            <div class="box-body table-responsive no-padding">
                <div class="box-header customHeader">
                    <h3 class="box-title"><b>Adjuster Data</b></h3>
                    <a href="{{ route('adjusters.edit', $adjuster->id) }}" style="float:right; color:white;">Edit</a>
                </div>
              <table class="table table-bordered tableStyle">
               <tbody>
                <tr>
                  <th>Name</th>
                  <td>{{ $adjuster->getFullName() ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Email</th>
                	<td>{{ $adjuster->email ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Primary Phone Number</th>
                	<td>{{ $adjuster->primary_phone_no ?? "-" }}</td>
                </tr>
                 <tr>
                	<th>Secondary Phone Number</th>
                	<td>{{ $adjuster->secondary_phone_no ?? "-" }}</td>
                </tr>
                 <tr>
                	<th>Emergency Phone Number</th>
                	<td>{{ $adjuster->emergency_phone_no ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Date of Birth</th>
                	<td>{{ $adjuster->dob ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Drivers License</th>
                	<td>{{ $adjuster->driver_license ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Street Number Name</th>
                	<td>{{ $adjuster->street_number ?? "-" }}</td>
                </tr>
                <tr>
                	<th>City</th>
                	<td>{{ $adjuster->city->name ?? '-' }}</td>
                </tr>
                <tr>
                	<th>State</th>
                	<td>{{ $adjuster->state->name ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Zip</th>
                	<td>{{ $adjuster->zip ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Driver License State</th>
                	<td>{{ $adjuster->driver_license ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Company or LLC Name</th>
                	<td>{{ $adjuster->company_llc_name ?? "-" }}</td>
                </tr>
                <tr>
                	<th>FEIN</th>
                	<td>{{ $adjuster->company_tin ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Tax ID</th>
                	<td>{{ $adjuster->tx_id ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Fluent Languages </th>
                	<td>
                        @if($adjuster->fluent_languages)
                            @foreach(json_decode($adjuster->fluent_languages) as $language)
                                {{ $language }}
                                @if($loop->index >= 0 && $loop->index < count(json_decode($adjuster->fluent_languages)) - 1)
                                    ,
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>Xact Address</th>
                	<td>{{ $adjuster->xact_address ?? "-" }}</td>
                </tr>
                {{-- <tr>
                	<th>Primary Service State</th>
                	<td>{{ $adjuster->primary_service_area->name ?? '-' }}</td>
                </tr> --}}
                <tr>
                	<th>Service Area of State</th>
                	<td>{{ $adjuster->service_state_area->name ?? '-' }}</td>
                </tr>
                <tr>
                	<th>Current Active State </th>
                	<td>
                        @if($adjuster->current_active_state)
                            @foreach(json_decode($adjuster->current_active_state) as $state)
                                {{ $state }}
                                @if($loop->index >= 0 && $loop->index < count(json_decode($adjuster->current_active_state)) - 1)
                                    ,
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>Personnel Folder Status</th>
                	<td>{{ $adjuster->personal_folder_made ?? '-' }}</td>
                </tr>
                <tr>
                	<th>URL</th>
                	<td>
                        @if($adjuster->url == 'www.')
                            -
                        @else
                            <a href="https://{{ $adjuster->url }}" target="_blank">{{ $adjuster->url }}</a>
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>Specialties</th>
                	<td>
                        @if($adjuster->speciality)
                            @foreach(json_decode($adjuster->speciality) as $speciality)
                                {{ $state }}
                                @if($loop->index >= 0 && $loop->index < count(json_decode($adjuster->speciality)) - 1)
                                    ,
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>Background Check Date Complete</th>
                	<td>{{ $adjuster->check_background_date ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Adjuster Type </th>
                	<td>
                        @if(!empty($adjuster->types))
                            @foreach($adjuster->types as $type)
                                {{ $type->name }}
                                @if($loop->index >= 0 && $loop->index < $adjuster->types->count() - 1)
                                    ,
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>Adjuster Grade</th>
                	<td>{{ $adjuster->grade->name ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Grade Reason</th>
                	<td>{{ $adjuster->grade_reason->name ?? "-" }}</td>
                </tr>
                <tr>
                	<th>Role</th>
                	<td>{{ $adjuster->assign_role->name ?? '-'}}</td>
                </tr>
                <tr>
                	<th>States Licenses</th>
                	<td>
                        @if(!empty($adjuster->home_state_licenses))
                            @foreach($adjuster->home_state_licenses as $home_state_license)
                                {{ $home_state_license->state->name }}
                                @if($loop->index >= 0 && $loop->index < $adjuster->home_state_licenses->count() - 1)
                                    ,
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                	<th>InActive Reason</th>
                	<td>{{ $adjuster->inactive_reason->name ?? '-'}}</td>
                </tr>
                <tr>
                	<th>Initial TSI Service Date</th>
                	<td>{{ $adjuster->initial_service_date ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Comment</th>
                    @if($adjuster->is_flagged == 1)
                    <td style="color:red;">{{ $adjuster->comment ?? '--'}}</td>
                    @else
                    <td>{{ $adjuster->comment ?? '--'}}</td>
                    @endif
                </tr>
                @if($adjuster->is_flagged == 1)
                <tr>
                    <th>Check Date</th>
                    <td>{{ $adjuster->flag_check_date ?? '--'}}</td>
                </tr>
                @endif
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection

@push('css')
<style type="text/css">
.tableStyle th
{
    width: 30%;
}
</style>
@endpush

