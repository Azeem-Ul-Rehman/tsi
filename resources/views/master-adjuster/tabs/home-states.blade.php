@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title"><b>{{ $adjuster->getFullName() ?? "-" }} - {{ $adjuster->state->name ?? "-" }}</b></h3>
            <a href="{{ route('adjusters.edit', $adjuster->id) }}" style="float:right; color:white;">Edit</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adjuster->id) }}"  class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">State Licenses</a>
            <a href="{{ route('tab.adjuster.corrections', $adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            <a href="{{ route('tab.adjuster.log-assignments', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        <div class="clearfix"></div>
        <div class="box-body table-responsive no-padding">
            <div class="box-header customHeader">
                    <h3 class="box-title"><b>State Licenses</b></h3>
            </div>
            <table class="table table-bordered tableStyle">
                <tbody>
                <tr>
                    <th>First Name</th>
                    <td>{{ $adjuster->first_name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{{ $adjuster->last_name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date of Birth</th>
                    <td>{{ $adjuster->dob ?? "-" }}</td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>{{ $adjuster->city->name ?? '-' }}</td>
                </tr>
                <tr>
                    <th>State</th>
                    <td>{{ $adjuster->state->name ?? "-" }}</td>
                </tr>
                </tbody>
            </table>
            @if(!empty($adjuster->home_state_licenses))
                @foreach($adjuster->home_state_licenses as $state_license)
                    <div class="box-header customHeader">
                        <h3 class="box-title"><b>State: {{ $state_license->state->name ?? "-" }}</b></h3>
                        <a href="{{ route('home-state-license-single.edit', $state_license->id) }}" style="float:right; color:white;">Edit</a>
                    </div>
                <table class="table table-bordered tableStyle">
                    <tbody>
                        <tr>
                            <th>License State</th>
                            <td>{{ $state_license->state->name ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>License Number</th>
                            <td>{{ $state_license->license_number ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Expiry Data</th>
                            <td>{{ $state_license->expiry_date ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>NPN Number</th>
                            <td>{{ $state_license->npn_number ?? '-' }}</td>
                        </tr>
                        <tr>
                            <th>Home State</th>
                            <td>{{ ($state_license->home_state === 1) ? 'Yes' : 'No' }}</td>
                        </tr>
                        <tr>
                            <th>Active/Not Active</th>
                            <td>{{ ($state_license->active === 1) ? 'Active' : 'Not Active' }}</td>
                        </tr>
                        <tr>
                            <th>Comment</th>
                            <td>{{ $state_license->comment ?? "-" }}</td>
                        </tr>
                    </tbody>
                </table>
            @endforeach
            @endif
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')
    <style type="text/css">
        .tableStyle th
        {
            width: 20%;
        }
    </style>
@endpush

