@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title"><b>{{ $adjuster->getFullName() ?? "-" }} - {{ $adjuster->state->name ?? "-" }}</b>
            </h3>
            <a href="{{ route('adjusters.show', $adjuster->id) }}" style="float:right; color:white;">View</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adjuster->id) }}"
               style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;"
               class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adjuster->id) }}" class="btn btn-primary btnTheme">State
                Licenses</a>
            <a href="{{ route('tab.adjuster.corrections', $adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adjuster->id) }}"
               class="btn btn-primary btnTheme">Rejections</a>
            <a href="{{ route('tab.adjuster.log-assignments', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim
                Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim
                Status</a>
            {{-- <div class="col-md-2"></div> --}}
        </div>
        <div class="clearfix"></div>
        @if($adjuster->is_flagged == 1)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        Adjuster Flagged. See Comment
                    </div>
                </div>
            </div>
        @endif
        <form action="{{ route('adjusters.update', $adjuster->id) }}" method="POST" id="main-form">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="box-header customHeader">
                    <h3 class="box-title"><b>Adjuster Data</b></h3>
                    <a href="{{ route('adjusters.show', $adjuster->id) }}" style="float:right; color:white;">View</a>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control"
                                       value="{{ $adjuster->first_name }}" required="">
                                @if($errors->has('first_name'))
                                    <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control"
                                       value="{{ $adjuster->last_name }}"
                                       required="">
                                @if($errors->has('last_name'))
                                    <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ $adjuster->email }}"
                                       required="">
                                @if($errors->has('email'))
                                    <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error': '' }}">
                                <label>Primary Phone Number</label>
                                <input type="tel" name="primary_phone_number" class="form-control"
                                       value="{{ $adjuster->primary_phone_no }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('primary_phone_number'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error': '' }}">
                                <label>Secondary Phone Number</label>
                                <input type="tel" name="secondary_phone_number" class="form-control"
                                       value="{{ $adjuster->secondary_phone_no }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('secondary_phone_number'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('emergency_contact_name') ? 'has-error': '' }}">
                                <label>Emergency Contact Name</label>
                                <input type="text" name="emergency_contact_name" class="form-control"
                                       value="{{ $adjuster->emergency_contact_name }}">
                                @if($errors->has('emergency_contact_name'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('emergency_contact_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('emergency_contact_phone') ? 'has-error': '' }}">
                                <label>Emergency Contact Phone</label>
                                <input type="tel" name="emergency_contact_phone" class="form-control"
                                       value="{{ $adjuster->emergency_phone_no }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('emergency_contact_phone'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('emergency_contact_phone') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('dob') ? 'has-error': '' }}">
                                <label>Date of Birth</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="dob" class="form-control datepicker"
                                           value="{{ $adjuster->dob }}">
                                    @if($errors->has('dob'))
                                        <span class="help-block text-danger">{{ $errors->first('dob') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('driver_license') ? 'has-error': '' }}">
                                <label>Drivers License</label>
                                <input type="text" name="driver_license" class="form-control"
                                       value="{{ $adjuster->driver_license }}">
                                @if($errors->has('driver_license'))
                                    <span class="help-block text-danger">{{ $errors->first('driver_license') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('street_number_name') ? 'has-error': '' }}">
                                <label>Street Number Name</label>
                                <input type="text" name="street_number_name" class="form-control"
                                       value="{{ $adjuster->street_number }}">
                                @if($errors->has('street_number_name'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('street_number_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                                <label>State</label>
                                <select class="form-control" name="state_id" id="all_states">
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option
                                                value="{{ $state->id }}" {{ ($adjuster->state_id == $state->id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('state_id'))
                                    <span class="help-block text-danger">{{ $errors->first('state_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                                <label>City</label>
                                <select name="city" class="form-control" id="cities">
                                    @foreach($adjuster->state->cities as $city)
                                        <option value="{{ $city->id}}">{{ $city->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('city'))
                                    <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                                <label>Zip</label>
                                <input type="text" name="zip" class="form-control" value="{{ $adjuster->zip}}">
                                @if($errors->has('zip'))
                                    <span class="help-block text-danger">{{ $errors->first('zip') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('dl_state') ? 'has-error': '' }}">
                                <label>Driver License State</label>
                                <select class="form-control" name="dl_state">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option
                                                value="{{ $state->name }}" {{ ($adjuster->dl_state == $state->name) ? 'selected' : ''}}>{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('dl_state'))
                                    <span class="help-block text-danger">{{ $errors->first('dl_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('company_llc_name') ? 'has-error': '' }}">
                                <label>Company or LLC Name</label>
                                <input type="text" name="company_llc_name" class="form-control"
                                       value="{{ $adjuster->company_llc_name }}">
                                @if($errors->has('company_llc_name'))
                                    <span class="help-block text-danger">{{ $errors->first('company_llc_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('company_tin') ? 'has-error': '' }}">
                                <label>FEIN</label>
                                <input type="text" name="company_tin" class="form-control"
                                       value="{{ $adjuster->company_tin }}">
                                @if($errors->has('company_tin'))
                                    <span class="help-block text-danger">{{ $errors->first('company_tin') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('tax_idds') ? 'has-error': '' }}">
                                <label>Social Security Number</label>
                                <input type="text" name="tax_idds" class="form-control" value="{{ $adjuster->tx_id }}"
                                       id="tax_id">
                                @if($errors->has('tax_idds'))
                                    <span class="help-block text-danger">{{ $errors->first('tax_idds') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div
                                class="form-group {{ $errors->has('initials_of_fluent_languages') ? 'has-error': '' }}">
                                <label>Fluent Languages</label>
                                <select name="fluent_languages[]" class="select2 form-control" multiple>
                                    @if(!empty($languages))
                                        @foreach($languages as $language)
                                            <option value="{{ $language->name }}"
                                                    @if($adjuster->fluent_languages)
                                                    @foreach(json_decode($adjuster->fluent_languages) as $fluent_language)
                                                    @if($fluent_language == $language->name)
                                                    selected
                                                @endif
                                                @endforeach
                                                @endif
                                            >{{ $language->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('initials_of_fluent_languages'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('initials_of_fluent_languages') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('exact_address') ? 'has-error': '' }}">
                                <label>Xact Address</label>
                                <input type="text" name="exact_address" class="form-control"
                                       value="{{ $adjuster->xact_address }}">
                                @if($errors->has('exact_address'))
                                    <span class="help-block text-danger">{{ $errors->first('exact_address') }}</span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="col-md-12">
                            <div class="form-group {{ $errors->has('primary_serv_state') ? 'has-error': '' }}">
                                <label>Primary Service State</label>
                                <select class="form-control" name="primary_serv_state">
                                    <option value="">--Select--</option>
                                    @if(!empty($state_service_areas))
                                        @foreach($state_service_areas as $state_service_area)
                                            <option
                                                value="{{ $state_service_area->id }}" {{ ($adjuster->state_service_area_id == $state_service_area->id) ? 'selected' : '' }}>{{ $state_service_area->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('primary_serv_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('primary_serv_state') }}</span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('service_area_of_state') ? 'has-error': '' }}">
                                <label>Service Area of State</label>
                                <select class="form-control" name="service_area_of_state">
                                    <@if(!empty($state_service_areas_t1))
                                        @foreach($state_service_areas_t1 as $state_service_area_t1)
                                            <option
                                                value="{{ $state_service_area_t1->id }}" {{ ($adjuster->state_service_area_t1_id == $state_service_area_t1->id) ? 'selected' : '' }}>{{ $state_service_area_t1->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('service_area_of_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('service_area_of_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('current_active_state') ? 'has-error': '' }}">
                                <label>Current Active State</label>
                                <select class="form-control select2" name="current_active_state[]" multiple>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->name }}"
                                                    @if($adjuster->current_active_state)
                                                    @foreach(json_decode($adjuster->current_active_state) as $adjuster_active_state)
                                                    @if($adjuster_active_state == $state->name)
                                                    selected
                                                @endif
                                                @endforeach
                                                @endif
                                            >{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('current_active_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('current_active_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('personnel_folder_made') ? 'has-error': '' }}">
                                <label>Personnel Folder Status</label>
                                <select class="form-control" name="personnel_folder_made">
                                    @if(!empty($personal_statuses))
                                        @foreach($personal_statuses as $personal_status)
                                            <option
                                                value="{{ $personal_status->name }}" {{($adjuster->personal_folder_made == $personal_status->name) ? 'selected' : '' }}>{{ $personal_status->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('personnel_folder_made'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('personnel_folder_made') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('url') ? 'has-error': '' }}">
                                <label>URL</label>
                                <div class="input-group">
                                    <span class="input-group-addon">www.</span>
                                    <input type="text" class="form-control" name="url"
                                           value="{{ $adjuster->getURL() }}">
                                </div>
                                @if($errors->has('url'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('url') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('specialities') ? 'has-error': '' }}">
                                <label>Specialties</label>
                                <select name="specialities[]" class="form-control select2" multiple>
                                    @if(!empty($specialities))
                                        @foreach($specialities as $speciality)
                                            <option value="{{ $speciality->name }}"
                                                    @if($adjuster->speciality)
                                                    @foreach (json_decode($adjuster->speciality) as $spec)
                                                    @if($spec == $speciality->name)
                                                    selected
                                                @endif
                                                @endforeach
                                                @endif
                                            >{{ $speciality->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('specialities'))
                                    <span class="help-block text-danger">{{ $errors->first('specialities') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div
                                class="form-group {{ $errors->has('date_background_check_complete') ? 'has-error': '' }}">
                                <label>Background Check Date Complete</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datepicker"
                                           name="date_background_check_complete"
                                           value="{{ $adjuster->check_background_date }}">
                                    @if($errors->has('date_background_check_complete'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('date_background_check_complete') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('adjuster_types') ? 'has-error': '' }}">
                                    <label>Adjuster Type</label>
                                    <select class="form-control select2" name="adjuster_types[]" multiple>
                                        <option value="">--Select--</option>
                                        @if(!empty($adjuster_types))
                                            @foreach($adjuster_types as $type)
                                                <option value="{{ $type->id }}"
                                                        @if(!empty($adjuster->types))
                                                        @foreach ($adjuster->types as $ad_type)
                                                        @if($type->id == $ad_type->id)
                                                        selected
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                >{{ $type->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('adjuster_types'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('adjuster_types') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('adjuster_types') ? 'has-error': '' }}">
                                    <label>Adjuster Grade</label>
                                    <select class="form-control" name="adjuster_grade">
                                        <option value="">--Select--</option>
                                        @if(!empty($adjuster_grades))
                                            @foreach($adjuster_grades as $grade)
                                                <option
                                                    value="{{ $grade->id }}" {{ ($adjuster->adjuster_grade_id == $grade->id) ? 'selected' : ''}}>{{ $grade->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('adjuster_grade'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('adjuster_grade') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('adjuster_types') ? 'has-error': '' }}">
                                    <label>Grade Reason</label>
                                    <select class="form-control" name="grade_reason">
                                        <option value="">--Select--</option>
                                        @if(!empty($grade_reasons))
                                            @foreach($grade_reasons as $reason)
                                                <option
                                                    value="{{ $reason->id }}" {{ ($adjuster->grade_reason_id == $reason->id) ? 'selected' : ''}}>{{ $reason->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('grade_reason'))
                                        <span class="help-block text-danger">{{ $errors->first('grade_reason') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('grade_comments') ? 'has-error': '' }}">
                                    <label>Grade Comments</label>
                                    <textarea name="grade_comments" id="grade_comments" class="form-control" cols="30"
                                              rows="5">{{$adjuster->grade_comments}}</textarea>
                                    @if($errors->has('grade_comments'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('grade_comments') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="">Certifications</label>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-primary btn-sm btn-block" type="button"
                                                data-toggle="modal"
                                                data-target="#mymodal">Add
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-success btn-sm btn-block"
                                                id="edit-certificate">Edit
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-danger btn-sm btn-block"
                                                id="delete-certificate">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="certiContent">
                                    <div id="certificate-container">
                                        @if(!empty($adjuster->certificates))
                                            @foreach($adjuster->certificates as $certificate)
                                                <div class="single-container" id="single-container-{{ $loop->index }}1">
                                                    <input type="radio" class="single-certificate"
                                                           name="single-certificate"
                                                           value="{{ $loop->index }}1"> &nbsp; {{ $certificate->name }}
                                                    | {{ $certificate->expiry_date }}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('assign_type') ? 'has-error': '' }}">
                                    <label>Role</label>
                                    <select class="form-control" name="assign_role">
                                        <option value="">--Role--</option>
                                        @if(!empty($assign_roles))
                                            @foreach($assign_roles as $assign_role)
                                                <option
                                                    value="{{ $assign_role->id }}" {{ $adjuster->assign_role_id == $assign_role->id ? 'selected' : '' }}>{{ $assign_role->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('assign_type'))
                                        <span class="help-block text-danger">{{ $errors->first('assign_type') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="">State License</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-primary btn-sm btn-block" type="button"
                                                data-toggle="modal"
                                                data-target="#license-modal">Add
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-success btn-sm btn-block"
                                                id="edit-license-state-btn">Edit
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-danger btn-sm btn-block"
                                                id="delete-license-state">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="certiContent">
                                    <div id="license-container">
                                        @if(!empty($adjuster->home_state_licenses))
                                            @foreach ($adjuster->home_state_licenses as $license_state)
                                                <div class="single-license-container"
                                                     id="single-license-{{ $loop->iteration }}">
                                                    <input type="radio" class="single-license" name="state-license"
                                                           value="{{ $loop->iteration }}">{{ $license_state->state->name .' | '. $license_state->license_number .' | '. $license_state->expiry_date}}
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('npn_number') ? 'has-error': '' }}">
                                    <label>NPN Number</label>
                                    <input type="text" name="npn_number" class="form-control"
                                           value="{{ $adjuster->npn_number }}">
                                    @if($errors->has('npn_number'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('exact_address') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('inactive_rsn') ? 'has-error': '' }}">
                                    <label>Inactive Reason</label>
                                    @if($adjuster->inactive_start_date || $adjuster->inactive_end_date)
                                        <a href="javascript:void(0);" style="float:right;"
                                           id="open-inactive-date-modal">Date of Return</a>
                                    @endif
                                    <select class="form-control" name="inactive_rsn" id="inactive-reason">
                                        <option value="">--Select--</option>
                                        @if(!empty($personal_inactive_reasons))
                                            @foreach($personal_inactive_reasons as $personal_inactive_reason)
                                                <option
                                                    value="{{ $personal_inactive_reason->id }}" {{ ($adjuster->personal_inactive_reason_id == $personal_inactive_reason->id) ? 'selected' : '' }} >{{ $personal_inactive_reason->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('inactive_rsn'))
                                        <span class="help-block text-danger">{{ $errors->first('inactive_rsn') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('initial_service_date') ? 'has-error': '' }}">
                                    <label>Initial TSI Service Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="initial_service_date" class="form-control datepicker"
                                               value="{{ $adjuster->initial_service_date }}">
                                        @if($errors->has('initial_service_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('initial_service_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="comment">Comments</label><br>
                                <textarea name="comment" class="form-control"
                                          id="comment">{{ $adjuster->comment }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group" style="margin-top:20px;">
                                <input type="checkbox" name="flagged" id="flagged" value="1"
                                       {{ ($adjuster->is_flagged == 1) ? 'checked' : '' }} onclick="checkDate()"> <b>Flagged</b>
                            </div>
                        </div>
                        <div class="col-md-3" {{ ($adjuster->is_flagged == 1) ? '' : 'hidden' }} id="flag_check_date">
                            <label for="">Date of Return</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control flag_check_date datepicker"
                                       name="flag_check_date" required="" value="{{$adjuster->flag_check_date??''}}">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="hidden-inputs">
                <div id="license-home-states">

                    @if(!empty($adjuster->home_state_licenses))
                        @foreach($adjuster->home_state_licenses as $license)
                            <div id="license-{{ $loop->iteration }}">
                                <input type="hidden" name='license_states[]' value="{{ $license->state_id }}">
                                <input type="hidden" name='license_expiry_dates[]' value="{{ $license->expiry_date }}">
                                <input type="hidden" name='license_numbers[]' value="{{ $license->license_number }}">
                                <input type="hidden" name='license_comments[]' value="{{ $license->comment }}">
                            </div>
                        @endforeach
                    @endif
                </div>
                <div id="certificate-hidden-input">
                    @if(!empty($adjuster->certificates))
                        @foreach($adjuster->certificates as $certificate)
                            <div id="certificate-hidden-input-set-{{ $loop->index }}1">
                                <input type="hidden" name="certificate_names[]" value="{{ $certificate->name }}">
                                <input type="hidden" name="certificate_expiry_dates[]"
                                       value="{{ $certificate->expiry_date }}">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btnTheme">Update</button>
                </div>
            </div>
        </form>
    </div>

    {{-- Certificate Modal--}}
    <div class="modal fade in" id="mymodal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Certification</h4>
                </div>
                <div class="modal-body" id="certificate-wrapper">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Certificate</label>
                                <input type="text" class="form-control certificates" name="certificates">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <label for="">Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">
                            </div>
                        </div>

                        <div class="col-md-2" style="margin-top:30px;">
                            <button class="btn btn-primary btn-sm" id="add-more"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-certificate">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Edit Certificate Modal --}}
    <div class="modal fade in" id="edit-certificate-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit Certificate</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="edit-tracker" value="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Certification</label>
                                <input type="text" class="form-control" id="edit-certificate-input" name="certificates">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <label for="">Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control edit-expiry-date datepicker"
                                       id="edit-expiry-date-input"
                                       name="expiry_dates">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="edit-certificate-done">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- License state Modal--}}
    <div class="modal fade in" id="license-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add License State</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">State</label>
                                <select class="form-control" id="license-state">
                                    <option value="">--Select</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block text-danger" style="display: none; color:red;">State field is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">License Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="license-expiry-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">Expiry Date is required.</span>
                            </div>
                        </div>

                        <div class="row"></div>
                        <div class="col-md-6">
                            <label for="">License Number</label>
                            <input type="text" id="license-number" class="form-control">
                            <span class="help-block text-danger" style="display: none; color:red;">License number is required.</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="license-comment">Comment</label><br>
                            <textarea id="license-comment" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-license-state">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Edit License state Modal--}}
    <div class="modal fade in" id="edit-license-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit License State</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="edit-tracker-license" value="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">State</label>
                                <select class="form-control" id="edit-license-state">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block text-danger" style="display: none; color:red;">State field is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">License Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="edit-license-expiry-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">Expiry Date is required.</span>
                            </div>
                        </div>

                        <div class="row"></div>
                        <div class="col-md-6">
                            <label for="">License Number</label>
                            <input type="text" id="edit-license-number" class="form-control">
                            <span class="help-block text-danger" style="display: none; color:red;">License number is required.</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="license-comment">Comment</label><br>
                            <textarea id="edit-license-comment" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="edit-license-state-done">Edit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Inactive Reason Date Model --}}
    <div class="modal fade in" id="inactive-reason-date-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Inactive Dates</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Start Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="start-date" class="form-control datepicker"
                                       value="{{ $adjuster->inactive_start_date}}">
                                <span class="help-block text-danger" style="display: none; color:red;">Start Date is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">End Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="end-date" class="form-control datepicker"
                                       value="{{ $adjuster->inactive_end_date}}">
                                <span class="help-block text-danger" style="display: none; color:red;">End Date is required.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-inactive-reason-date">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            //setting the state license tracker
            var tracker = {{ $adjuster->home_state_licenses->count()}};
            localStorage.setItem('license_tracker', tracker);
            var dataBefore = $("#main-form").serialize();
            $(window).bind('beforeunload', function (e) {
                if (dataBefore != $("#main-form").serialize()) {
                    return "You made some changes and it's not saved?";
                } else {
                    e = null;
                }
            });
            $(document).on("submit", "form", function (event) {
                // disable warning
                $(window).off('beforeunload');
            });

            $('#dataType').change(function () {
                var data_type = $(this).val();
                switch (data_type) {
                    case 'field':
                        $('.fieldDataInputs').fadeIn('slow');
                        $('.staffDataInputs').fadeOut('slow');
                        break;
                    case 'staff':
                        $('.fieldDataInputs').fadeOut('slow');
                        $('.staffDataInputs').fadeIn('slow');
                        break;
                }
            });

            $('.select2').select2();

            var x = 1;
            var html = '<div class="row">\n' +
                '                        <div class="col-md-5">\n' +
                '                            <div class="form-group">\n' +
                '                                <label for="">Certification</label>\n' +
                '                                <input type="text" class="form-control certificates" name="certificates">\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '\n' +
                '                        <div class="col-md-5">\n' +
                '                                <label for="">Expiry Date</label>\n' +
                '                            <div class="input-group">\n' +
                '                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>\n' +
                '                           <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">\n' +
                '                        </div> </div>\n' +
                '\n' +
                '                        <div class="col-md-2" style="margin-top:30px;">\n' +
                '                            <button class="btn btn-danger btn-sm remove-field" type="button"><i class="fa fa-minus"></i></button>\n' +
                '                        </div>\n' +
                '                    </div>';

            $("#add-more").click(function () {
                if (x < 11) {
                    $("#certificate-wrapper").append(html);
                    x++;
                    $('.datepicker').datepicker({
                        autoclose: true,
                        format: 'mm-dd-yyyy'
                    });
                }
            });

            $("#certificate-wrapper").on('click', '.remove-field', function () {
                $(this).closest('div .row').remove();
                x--;
            });

            //add certificate
            $("#add-certificate").click(function () {
                var certificate_html = '';
                var certificate_input_html = '';
                var certificate_names = [];
                var expiry_dates = [];
                $(".certificates").each(function () {
                    certificate_names.push($(this).val());
                });

                $('.expiry-dates').each(function () {
                    expiry_dates.push($(this).val());
                });

                var new_counter = {{ $adjuster->certificates->count() }};

                $(certificate_names).each(function (i, d) {
                    certificate_html += '<div class="single-container" id="single-container-' + new_counter + 1 + '"><input type="radio" name="single-certificate" class="single-certificate" value="' + new_counter + 1 + '"> &nbsp; ' + d + ' | ' + expiry_dates[i] + '</div>';
                    certificate_input_html += '<div id="certificate-hidden-input-set-' + new_counter + 1 + '"><input type="hidden" class="certificate-name" name="certificate_names[]" value="' + d + '">';
                    certificate_input_html += '<input type="hidden" class="expiry-date" name="certificate_expiry_dates[]" value="' + expiry_dates[i] + '"></div>';
                    new_counter++;
                });

                $("#certificate-container").append(certificate_html);
                $("#certificate-hidden-input").append(certificate_input_html);

                //change the modal input to default
                $("#certificate-wrapper").html('<div class="row">\n' +
                    '                        <div class="col-md-5">\n' +
                    '                            <div class="form-group">\n' +
                    '                                <label for="">Certificate</label>\n' +
                    '                                <input type="text" class="form-control certificates" name="certificates">\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-md-5">\n' +
                    '                            <label for="">Expiry Date</label>\n' +
                    '                            <div class="input-group">\n' +
                    '                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>\n' +
                    '                                <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">\n' +
                    '\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <div class="col-md-2" style="margin-top:30px;">\n' +
                    '                            <button class="btn btn-primary btn-sm" id="add-more"><i class="fa fa-plus"></i></button>\n' +
                    '                        </div>\n' +
                    '                    </div>');

                //closing the modal
                $("#mymodal").modal('hide');
            });
        });

        //setting the selected certificate details in edit inputs
        $("#edit-certificate").click(function () {
            if ($(".single-certificate:checked").length > 0) {
                $("#edit-certificate-modal").modal('show');
                var need_to_be_edit = $(".single-certificate:checked").val();
                var certificate_name_input = $($("#certificate-hidden-input-set-" + need_to_be_edit + " > :input")[0]).val();
                var expiry_date_input = $($("#certificate-hidden-input-set-" + need_to_be_edit + " > :input")[1]).val();

                $("#edit-certificate-input").val(certificate_name_input);
                $("#edit-expiry-date-input").val(expiry_date_input);
                $("#edit-tracker").val(need_to_be_edit);
            } else {
                alert('Please select the certificate.');
            }
        });

        $("#edit-certificate-done").click(function () {
            var certificate_name = $("#edit-certificate-input").val();
            var expiry_date = $("#edit-expiry-date-input").val();
            var edit_tacker = $("#edit-tracker").val();

            certificate_input_html = '<input type="hidden" class="certificate-name" name="certificate_names[]" value="' + certificate_name + '">';
            certificate_input_html += '<input type="hidden" class="expiry-date" name="certificate_expiry_dates[]" value="' + expiry_date + '">';
            $("#certificate-hidden-input-set-" + edit_tacker).html(certificate_input_html);
            $("#single-container-" + edit_tacker).html('<input type="radio" name="state-license" class="single-certificate" value="' + edit_tacker + '">&nbsp;' + certificate_name + ' | ' + expiry_date);
            $("#edit-certificate-modal").modal('hide');
        });

        //delete the specific single certificate
        $("#delete-certificate").click(function () {
            if ($(".single-certificate:checked").length > 0) {
                var need_to_be_delete = $(".single-certificate:checked").val();
                $("#certificate-hidden-input-set-" + need_to_be_delete).remove();
                $("#single-container-" + need_to_be_delete).remove();
            } else {
                alert('Please select the certificate.');
            }
        });

        //-- Add State License
        $("#add-license-state").click(function () {
            var state = $("#license-state");
            var state_name = $('#license-state option:selected').text();
            var expiry_date = $("#license-expiry-date");
            var license_number = $("#license-number");
            var license_comment = $("#license-comment");
            if (state.val().length == 0) {
                state.next('span').fadeIn(200);
            } else if (expiry_date.val().length == 0) {
                expiry_date.next('span').fadeIn(200);
            } else if (license_number.val().length == 0) {
                license_number.next('span').fadeIn(200);
            } else {
                var html = '';
                var license_radio = '';
                var license_tracker = localStorage.getItem('license_tracker') ? (parseInt(localStorage.getItem('license_tracker')) + 1) : 1;
                html = '<div id="license-' + license_tracker + '">';
                html += "<input type='hidden' name='license_states[]' value='" + state.val() + "'>";
                html += "<input type='hidden' name='license_expiry_dates[]' value='" + expiry_date.val() + "'>";
                html += "<input type='hidden' name='license_numbers[]' value='" + license_number.val() + "'>";
                html += "<input type='hidden' name='license_comments[]' value='" + license_comment.val() + "'>";
                html += "</div>";

                //making the license radio input
                license_radio = '<div class="single-container" id="single-license-' + license_tracker + '"><input type="radio" name="state-license" class="single-license" value="' + license_tracker + '">&nbsp;' + state_name + ' | ' + license_number.val() + ' | ' + expiry_date.val() + '</div>';
                $("#license-home-states").append(html);
                $("#license-container").append(license_radio);

                //storing license tracker in browser cache
                localStorage.setItem('license_tracker', license_tracker);

                //clearing the inputs
                state.prop('selectedIndex', 0);
                expiry_date.val('00/00/0000');
                license_number.val('');
                license_comment.val('');

                //closing the modal
                $("#license-modal").modal('hide');
            }
        });

        //-- Edit State License
        $("#edit-license-state-btn").click(function () {
            if ($(".single-license:checked").length > 0) {
                $("#edit-license-modal").modal('show');
                var need_to_be_edit = $(".single-license:checked").val();
                var state = $($("#license-" + need_to_be_edit + "> input")[0]).val();
                var expiry_date = $($("#license-" + need_to_be_edit + "> input")[1]).val();
                var license_number = $($("#license-" + need_to_be_edit + "> input")[2]).val();
                var license_comment = $($("#license-" + need_to_be_edit + "> input")[3]).val();

                // //maintain the edit license tracker
                // $("#edit-license-tracker").val(need_to_be_edit);
                console.log(state);
                //assign value to modal inputs
                $("#edit-license-state option").each(function () {
                    if ($(this).val() == state) {
                        $(this).attr('selected', true);
                    }
                });
                $("#edit-license-expiry-date").val(expiry_date);
                $("#edit-license-number").val(license_number);
                $("#edit-license-comment").val(license_comment);
            } else {
                alert('Please select the license state.')
            }
        });

        //-- Done Edit State License
        $("#edit-license-state-done").click(function () {
            var state = $("#edit-license-state");
            var state_name = $('#edit-license-state option:selected').text();
            var expiry_date = $("#edit-license-expiry-date");
            var license_number = $("#edit-license-number");
            var license_comment = $("#edit-license-comment");
            if (state.val().length == 0) {
                state.next('span').fadeIn(200);
            } else if (expiry_date.val().length == 0) {
                expiry_date.next('span').fadeIn(200);
            } else if (license_number.val().length == 0) {
                license_number.next('span').fadeIn(200);
            } else {
                var html = '';
                var license_radio = '';
                var license_tracker = localStorage.getItem('license_tracker');
                html += "<input type='hidden' name='license_states[]' value='" + state.val() + "'>";
                html += "<input type='hidden' name='license_expiry_dates[]' value='" + expiry_date.val() + "'>";
                html += "<input type='hidden' name='license_numbers[]' value='" + license_number.val() + "'>";
                html += "<input type='hidden' name='license_comments[]' value='" + license_comment.val() + "'>";

                //making the license radio input
                license_radio = '<input type="radio" class="single-license" value="' + license_tracker + '">' + state_name + ' - ' + license_number.val();
                $("#license-" + license_tracker).html(html);
                $("#single-license-" + license_tracker).html(license_radio);


                //clearing the inputs
                state.prop('selectedIndex', 0);
                expiry_date.val('00/00/0000');
                license_number.val('');
                license_comment.val('');

                //closing the modal
                $("#edit-license-modal").modal('hide');
            }
        });

        //delete the specific single certificate
        $("#delete-license-state").click(function () {
            if ($(".single-license:checked").length > 0) {
                var need_to_be_delete = $(".single-license:checked").val();
                $("#single-license-" + need_to_be_delete).remove();
                $("#license-" + need_to_be_delete).remove();
            } else {
                alert('Please select the certificate.');
            }
        });

        $("#open-inactive-date-modal").click(function () {
            $("#inactive-reason-date-modal").modal('show');
        });

        $("#inactive-reason").change(function () {
            if ($(this).val() != '') {
                //open modal
                $('#inactive-reason-date-modal').modal('show');
            }
        });

        $("#save-inactive-reason-date").click(function () {
            var start_date = $("#start-date");
            var end_date = $("#end-date");
            if (start_date.val().length == 0) {
                start_date.next('span').fadeIn(200);
            } else if (end_date.val().length == 0) {
                end_date.next('span').fadeIn(200);
            } else {
                $("#start-date-hidden").val(start_date.val());
                $("#end-date-hidden").val(end_date.val());

                //close the modal
                $("#inactive-reason-date-modal").modal('hide');
            }
        });

        $("#all_states").change(function () {
            var state = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{ route('get.cities') }}",
                data: {state_id: state, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        var options = '';
                        options += '<option value="">--Select--</option>';
                        response.cities.forEach(function (data) {
                            options += '<option value="' + data.id + '">' + data.name + '</option>';
                        });

                        $("#cities").html(options);
                    }
                }
            })
        });

        function checkDate() {
            if ($("#flagged").is(':checked')) {
                $("#flag_check_date").removeAttr('hidden');
            } else {
                $("#flag_check_date").attr('hidden', 'hidden');
            }

        }

        var tax_id = $('#tax_id').val();
        // $('#tax_id').on('keypress', function () {
        //     var value = $(this).val();
        //     var replaced = value.replace(/.(?=.{3,}$)/g, '*');
        //     $(this).val(replaced);
        // })
    </script>
@endpush
