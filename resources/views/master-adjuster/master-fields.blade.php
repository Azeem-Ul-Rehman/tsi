@extends('adminlte::page')
@section('title', 'Adjusters Field')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Adjusters Field</h3>
            </div>
            <div class="col-md-6 text-right">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form  method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="first_name" id="first_name" value="first_name" {{$getAllFields->first_name==1?'checked':''}} onchange="getvalue('first_name')"> Name
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="state" value="state" {{$getAllFields->state==1?'checked':''}} onchange="getvalue('state')"> State
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="assign_type_id" value="assign_type_id" {{$getAllFields->assign_type_id==1?'checked':''}} onchange="getvalue('assign_type_id')"> Type
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="city_id" value="city_id" {{$getAllFields->city_id==1?'checked':''}} onchange="getvalue('city_id')"> City
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="primary_phone_no" value="primary_phone_no" {{$getAllFields->primary_phone_no==1?'checked':''}} onchange="getvalue('primary_phone_no')"> Phone No.
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="email" value="email" {{$getAllFields->email==1?'checked':''}} onchange="getvalue('email')"> Email
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="initial_service_date" value="initial_service_date" {{$getAllFields->initial_service_date==1?'checked':''}} onchange="getvalue('initial_service_date')"> Initial Service Date
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="dob" value="dob" {{$getAllFields->dob==1?'checked':''}} onchange="getvalue('dob')"> DOB
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="secondary_phone_no" value="secondary_phone_no" {{$getAllFields->secondary_phone_no==1?'checked':''}} onchange="getvalue('secondary_phone_no')"> Secondary Phone No.
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="emergency_contact_name" value="emergency_contact_name" {{$getAllFields->emergency_contact_name==1?'checked':''}} onchange="getvalue('emergency_contact_name')"> Emergency Contact Name
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="emergency_phone_no" value="emergency_phone_no" {{$getAllFields->emergency_phone_no==1?'checked':''}} onchange="getvalue('emergency_phone_no')"> Emergency Phone No
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="street_number" value="street_number" {{$getAllFields->street_number==1?'checked':''}} onchange="getvalue('street_number')"> Street Number
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="zip" value="zip" {{$getAllFields->zip==1?'checked':''}} onchange="getvalue('zip')"> Zip
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="driver_license" value="driver_license" {{$getAllFields->driver_license==1?'checked':''}} onchange="getvalue('driver_license')"> Driver License
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="dl_state" value="dl_state" {{$getAllFields->dl_state==1?'checked':''}} onchange="getvalue('dl_state')"> Dl State
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="xact_address" value="xact_address" {{$getAllFields->xact_address==1?'checked':''}} onchange="getvalue('xact_address')"> Exact Address
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="tx_id" value="tx_id" {{$getAllFields->tx_id==1?'checked':''}} onchange="getvalue('tx_id')"> Tx Id
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="company_tin" value="company_tin" {{$getAllFields->company_tin==1?'checked':''}} onchange="getvalue('company_tin')"> Company Tin
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="company_llc_name" value="company_llc_name" {{$getAllFields->company_llc_name==1?'checked':''}} onchange="getvalue('company_llc_name')"> Company llc Name
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="state_license" value="state_license" {{$getAllFields->state_license==1?'checked':''}} onchange="getvalue('state_license')"> State License
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="speciality" value="speciality" {{$getAllFields->speciality==1?'checked':''}} onchange="getvalue('speciality')"> Speciality
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="personal_folder_made" value="personal_folder_made" {{$getAllFields->personal_folder_made==1?'checked':''}} onchange="getvalue('personal_folder_made')"> Personal Folder Made
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="fluent_languages" value="fluent_languages" {{$getAllFields->fluent_languages==1?'checked':''}} onchange="getvalue('fluent_languages')"> Fluent Languages
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="npn_number" value="npn_number" {{$getAllFields->npn_number==1?'checked':''}} onchange="getvalue('npn_number')"> Npn Number
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="check_background_date" value="check_background_date" {{$getAllFields->check_background_date==1?'checked':''}} onchange="getvalue('check_background_date')"> Check Background Date
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="is_flagged" value="is_flagged" {{$getAllFields->is_flagged==1?'checked':''}} onchange="getvalue('is_flagged')"> Flagged
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="comment" value="comment" {{$getAllFields->comment==1?'checked':''}} onchange="getvalue('comment')"> Comment
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="license_name" value="license_name" {{$getAllFields->license_name==1?'checked':''}} onchange="getvalue('license_name')"> license Name
                        </div>
                    </div>


                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function getvalue(Name){
            var status=0;
        if($("#"+Name+"").is(':checked')){
            status=1;
        }
           $.ajax({
                url: "{{ route('store') }}",
               type: 'POST',
               data : { status: status,name:Name},
               success: function(response){
                    if(response.status == 'success'){
                     toastr['success']("Master Field been updated.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        }
    </script>
@endpush
