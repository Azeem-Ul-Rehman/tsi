@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">Data Form</h3>
        </div>
        @if ($errors->any())
            <div>
                <div class="alert alert-danger alert-dismissible" role="alert" style="display: inline-block;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul style="list-style: none;">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <form action="{{ route('adjusters.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                                <label>First Name</label>
                                <input type="text" name="first_name" class="form-control"
                                       value="{{ old('first_name') }}" required="">
                                @if($errors->has('first_name'))
                                    <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                                <label>Last Name</label>
                                <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}"
                                       required="">
                                @if($errors->has('last_name'))
                                    <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('email') ? 'has-error': '' }}">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                       required="">
                                @if($errors->has('email'))
                                    <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('password') ? 'has-error': '' }}">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control"
                                       value="{{ old('password') }}"
                                       required="">
                                @if($errors->has('password'))
                                    <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error': '' }}">
                                <label>Confirm Password</label>
                                <input type="password" name="password_confirmation" class="form-control"
                                       value="{{ old('password_confirmation') }}"
                                       required="">
                                @if($errors->has('password_confirmation'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('primary_phone_number') ? 'has-error': '' }}">
                                <label>Primary Phone Number</label>
                                <input type="tel" name="primary_phone_number" class="form-control"
                                       value="{{ old('primary_phone_number') }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('primary_phone_number'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('primary_phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('secondary_phone_number') ? 'has-error': '' }}">
                                <label>Secondary Phone Number</label>
                                <input type="tel" name="secondary_phone_number" class="form-control"
                                       value="{{ old('secondary_phone_number') }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('secondary_phone_number'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('secondary_phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('emergency_contact_name') ? 'has-error': '' }}">
                                <label>Emergency Contact Name</label>
                                <input type="text" name="emergency_contact_name" class="form-control"
                                       value="{{ old('emergency_contact_name') }}">
                                @if($errors->has('emergency_contact_name'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('emergency_contact_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('emergency_contact_phone') ? 'has-error': '' }}">
                                <label>Emergency Contact Phone</label>
                                <input type="tel" name="emergency_contact_phone" class="form-control"
                                       value="{{ old('emergency_contact_phone') }}"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask>
                                @if($errors->has('emergency_contact_phone'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('emergency_contact_phone') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('dob') ? 'has-error': '' }}">
                                <label>Date of Birth</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="dob" class="form-control datepicker"
                                           value="{{ old('dob') }}">
                                    @if($errors->has('dob'))
                                        <span class="help-block text-danger">{{ $errors->first('dob') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('driver_license') ? 'has-error': '' }}">
                                <label>Drivers License</label>
                                <input type="text" class="form-control" name="driver_license"
                                       value="{{ old('driver_license') }}">
                                @if($errors->has('driver_license'))
                                    <span class="help-block text-danger">{{ $errors->first('driver_license') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('street_number_name') ? 'has-error': '' }}">
                                <label>Street Number Name</label>
                                <input type="text" name="street_number_name" class="form-control"
                                       value="{{ old('street_number_name') }}">
                                @if($errors->has('street_number_name'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('street_number_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                                <label>State</label>
                                <select class="form-control" name="state_id" id="all_states">
                                    <option value="" selected>Select</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            @if($state->id == old('state_id'))
                                                <option value="{{ $state->id }}"
                                                        selected="selected">{{ $state->name }}</option>
                                            @else
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('state_id'))
                                    <span class="help-block text-danger">{{ $errors->first('state_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                                <label>City</label>
                                <select name="city" class="form-control" id="cities">
                                    <option value="" selected>Select</option>
                                </select>
                                @if($errors->has('city'))
                                    <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                                <label>Zip</label>
                                <input type="text" name="zip" class="form-control" value="{{ old('zip') }}">
                                @if($errors->has('zip'))
                                    <span class="help-block text-danger">{{ $errors->first('zip') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('dl_state') ? 'has-error': '' }}">
                                <label>Driver License State</label>
                                <select class="form-control select2" name="dl_state" style="color:black;">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            @if($state->name== old('dl_state'))
                                                <option value="{{ $state->name }}"
                                                        selected="selected">{{ $state->name }}</option>
                                            @else
                                                <option value="{{ $state->name }}">{{ $state->name }}</option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('dl_state'))
                                    <span class="help-block text-danger">{{ $errors->first('dl_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('company_llc_name') ? 'has-error': '' }}">
                                <label>Company or LLC Name</label>
                                <input type="text" name="company_llc_name" class="form-control"
                                       value="{{ old('company_llc_name') }}">
                                @if($errors->has('company_llc_name'))
                                    <span class="help-block text-danger">{{ $errors->first('company_llc_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('company_tin') ? 'has-error': '' }}">
                                <label>FEIN</label>
                                <input type="text" name="company_tin" class="form-control"
                                       value="{{ old('company_tin') }}">
                                @if($errors->has('company_tin'))
                                    <span class="help-block text-danger">{{ $errors->first('company_tin') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('tax_idds') ? 'has-error': '' }}">
                                <label>Social Security Number</label>
                                <input type="text" name="tax_idds" class="form-control" value="{{ old('tax_idds') }}"
                                       id="tax_id">
                                @if($errors->has('tax_idds'))
                                    <span class="help-block text-danger">{{ $errors->first('tax_idds') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div
                                class="form-group {{ $errors->has('initials_of_fluent_languages') ? 'has-error': '' }}">
                                <label>Fluent Languages</label>
                                <select name="fluent_languages[]" class="select2 form-control" multiple>
                                    <option>--Select--</option>
                                    @if(!empty($languages))
                                        @foreach($languages as $language)
                                            <option
                                                value="{{ $language->name }}" {{!empty(old('fluent_languages')) && in_array($language->name, old('fluent_languages')) ? ' selected="selected"' : '' }}>{{ $language->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('initials_of_fluent_languages'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('initials_of_fluent_languages') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('exact_address') ? 'has-error': '' }}">
                                <label>Xact Address</label>
                                <input type="text" name="exact_address" class="form-control"
                                       value="{{ old('exact_address') }}">
                                @if($errors->has('exact_address'))
                                    <span class="help-block text-danger">{{ $errors->first('exact_address') }}</span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="col-md-12">
                            <div class="form-group {{ $errors->has('primary_serv_state') ? 'has-error': '' }}">
                                <label>Primary Service State</label>
                                <select class="form-control" name="primary_serv_state">
                                    <option value="">--Select--</option>
                                    @if(!empty($state_service_areas))
                                        @foreach($state_service_areas as $state_service_area)
                                            @if($state_service_area->id == old('primary_serv_state'))
                                                <option selected="selected"
                                                        value="{{ $state_service_area->id }}">{{ $state_service_area->name }}</option>
                                            @else
                                                <option
                                                    value="{{ $state_service_area->id }}">{{ $state_service_area->name }}</option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('primary_serv_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('primary_serv_state') }}</span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('service_area_of_state') ? 'has-error': '' }}">
                                <label>Service Area of State</label>
                                <select class="form-control" name="service_area_of_state">
                                    <@if(!empty($state_service_areas_t1))
                                        <option>Select</option>
                                        @foreach($state_service_areas_t1 as $state_service_area_t1)
                                            @if($state_service_area_t1->id == old('service_area_of_state'))
                                                <option
                                                    value="{{ $state_service_area_t1->id }}">{{ $state_service_area_t1->name }}</option>
                                            @else
                                                <option
                                                    value="{{ $state_service_area_t1->id }}">{{ $state_service_area_t1->name }}</option>
                                            @endif

                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('service_area_of_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('service_area_of_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('current_active_state') ? 'has-error': '' }}">
                                <label>Current Active State </label>
                                <select class="form-control select2" name="current_active_state[]" multiple>
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option
                                                value="{{ $state->name }}" {{ !empty(old('current_active_state')) && in_array($state->name, old('current_active_state')) ? ' selected="selected"' : '' }}>{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('current_active_state'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('current_active_state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('personnel_folder_made') ? 'has-error': '' }}">
                                <label>Personnel Folder Status</label>
                                <select class="form-control" name="personnel_folder_made">
                                    <option value="">--Select--</option>
                                    @if(!empty($personal_statuses))
                                        @foreach($personal_statuses as $personal_status)
                                            <option
                                                value="{{ $personal_status->name }}" {{(old('personal_folder_made') == $personal_status->name) ? 'selected' : '' }}>{{ $personal_status->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('personnel_folder_made'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('personnel_folder_made') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('url') ? 'has-error': '' }}">
                                <label>URL</label>
                                <div class="input-group">
                                    <span class="input-group-addon">www.</span>
                                    <input type="text" class="form-control" name="url" value="{{ old('url') }}">
                                </div>
                                @if($errors->has('url'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('url') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('specialties') ? 'has-error': '' }}">
                                <label>Specialties</label>
                                <select name="specialities[]" class="form-control select2" multiple>
                                    @if(!empty($specialities))
                                        @foreach($specialities as $speciality)
                                            <option
                                                value="{{ $speciality->name }}" {{ !empty(old('specialities')) && in_array($state->id, old('specialities')) ? ' selected="selected"' : '' }}>{{ $speciality->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('specialties'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('specialities') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div
                                class="form-group {{ $errors->has('check_background_date') ? 'has-error': '' }}">
                                <label>Background Check Date Complete</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datepicker"
                                           name="check_background_date"
                                           value="{{ old('check_background_date') }}">
                                    @if($errors->has('check_background_date'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('check_background_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('adjuster_types') ? 'has-error': '' }}">
                                    <label>Adjuster Type</label>
                                    <select class="form-control select2" name="adjuster_types[]" multiple>
                                        <option value="">--Select--</option>
                                        @if(!empty($adjuster_types))
                                            @foreach($adjuster_types as $type)
                                                <option
                                                    value="{{ $type->id }}" {{ !empty(old('adjuster_types')) && in_array($type->id, old('adjuster_types')) ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('adjuster_types'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('adjuster_types') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('adjuster_grade') ? 'has-error': '' }}">
                                    <label>Adjuster Grade</label>
                                    <select class="form-control" name="adjuster_grade">
                                        <option value="">--Select--</option>
                                        @if(!empty($adjuster_grades))
                                            @foreach($adjuster_grades as $grade)
                                                @if($grade->id == old('adjuster_grade'))
                                                    <option value="{{ $grade->id }}"
                                                            selected="selected">{{ $grade->name }}</option>
                                                @else
                                                    <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                                                @endif

                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('adjuster_grade'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('adjuster_grade') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('grade_reason') ? 'has-error': '' }}">
                                    <label>Grade Reason</label>
                                    <select class="form-control" name="grade_reason">
                                        <option value="">--Select--</option>
                                        @if(!empty($grade_reasons))
                                            @foreach($grade_reasons as $reason)
                                                @if($reason->id == old('grade_reason'))
                                                    <option value="{{ $reason->id }}"
                                                            selected="selected">{{ $reason->name }}</option>
                                                @else
                                                    <option value="{{ $reason->id }}">{{ $reason->name }}</option>
                                                @endif

                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('grade_reason'))
                                        <span class="help-block text-danger">{{ $errors->first('grade_reason') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group {{ $errors->has('grade_comments') ? 'has-error': '' }}">
                                    <label>Grade Comments</label>
                                    <textarea name="grade_comments" id="grade_comments" class="form-control" cols="30"
                                              rows="5"></textarea>
                                    @if($errors->has('grade_comments'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('grade_comments') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="">Certifications</label>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-primary btn-sm btn-block" type="button"
                                                data-toggle="modal"
                                                data-target="#mymodal">Add
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-success btn-sm btn-block"
                                                id="edit-certificate">Edit
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-danger btn-sm btn-block"
                                                id="delete-certificate">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="single-container">
                                    <div class="certiContent" id="certificate-container">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('assign_roles') ? 'has-error': '' }}">
                                    <label>Role</label>
                                    <select class="form-control" name="assign_role">
                                        <option value="">--Role--</option>
                                        @if(!empty($assign_roles))
                                            @foreach($assign_roles as $assign_role)
                                                @if($assign_role->id == old('assign_role'))
                                                    <option value="{{ $assign_role->id }}"
                                                            selected="selected">{{ $assign_role->name }}</option>
                                                @else
                                                    <option
                                                        value="{{ $assign_role->id }}">{{ $assign_role->name }}</option>
                                                @endif

                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('assign_type'))
                                        <span class="help-block text-danger">{{ $errors->first('assign_type') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="">State License</label>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-primary btn-sm btn-block" type="button"
                                                data-toggle="modal"
                                                data-target="#license-modal">Add
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-success btn-sm btn-block"
                                                id="edit-license-state-btn">Edit
                                        </button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="button" class="btn btn-danger btn-sm btn-block"
                                                id="delete-license-state">Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="single-license-container">
                                    <div class="certiContent" id="license-container">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('npn_number') ? 'has-error': '' }}">
                                    <label>NPN Number</label>
                                    <input type="text" name="npn_number" class="form-control"
                                           value="{{ old('npn_number') }}">
                                    @if($errors->has('npn_number'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('exact_address') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('inactive_rsn') ? 'has-error': '' }}">
                                    <label>Inactive/Do not used</label>
                                    <select class="form-control" name="inactive_rsn" id="inactive-reason">
                                        <option value="">--Select--</option>
                                        @if(!empty($personal_inactive_reasons))
                                            @foreach($personal_inactive_reasons as $personal_inactive_reason)
                                                @if($personal_inactive_reason->id == old('inactive_rsn'))
                                                    <option selected="selected"
                                                            value="{{ $personal_inactive_reason->id }}">{{ $personal_inactive_reason->name }}</option>
                                                @else
                                                    <option
                                                        value="{{ $personal_inactive_reason->id }}">{{ $personal_inactive_reason->name }}</option>
                                                @endif

                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('inactive_rsn'))
                                        <span class="help-block text-danger">{{ $errors->first('inactive_rsn') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('initial_service_date') ? 'has-error': '' }}">
                                    <label>Initial TSI Service Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" id="initial_service_date" name="initial_service_date"
                                               class="form-control datepicker"
                                               value="{{ old('initial_service_date') }}" data-date-inline-picker="true">
                                        @if($errors->has('initial_service_date'))
                                            <span
                                                class="help-block text-danger">{{ $errors->first('initial_service_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="comment">Comments</label><br>
                                <textarea name="comment" class="form-control" id="comment"></textarea>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group" style="margin-top:20px;">
                                <input type="checkbox" name="flagged" id="flagged" value="1" onclick="checkDate()"> <b>Flagged</b>
                            </div>
                        </div>
                        <div class="col-md-3" hidden="hidden" id="flag_check_date">
                            <label for="">Date of Return</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control flag_check_date datepicker"
                                       name="flag_check_date" required="">

                            </div>
                        </div>
                    </div>

                    <div id="hidden-inputs">
                        <div id="license-home-states"></div>
                        <div id="certificate-hidden-input"></div>
                        <input type="hidden" id="start-date-hidden" name="inactive_start_date">
                        <input type="hidden" id="end-date-hidden" name="inactive_end_date">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btnTheme">Save</button>
                </div>
            </div>
        </form>
    </div>

    {{-- Certificate Modal--}}
    <div class="modal fade in" id="mymodal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Certificate</h4>
                </div>
                <div class="modal-body" id="certificate-wrapper">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Certification</label>
                                <input type="text" class="form-control certificates" name="certificates">
                            </div>
                        </div>

                        <div class="col-md-5">
                            <label for="">Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">

                            </div>
                        </div>

                        <div class="col-md-2" style="margin-top:30px;">
                            <button class="btn btn-primary" id="add-more"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-certificate">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Edit Certificate Modal --}}
    <div class="modal fade in" id="edit-certificate-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit Certificate</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="edit-tracker" value="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Certificate</label>
                                <input type="text" class="form-control" id="edit-certificate-input" name="certificates">

                            </div>
                        </div>

                        <div class="col-md-5">
                            <label for="">Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control datepicker"
                                       id="edit-expiry-date-input"
                                       name="expiry_dates">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="edit-certificate-done">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- License state Modal--}}
    <div class="modal fade in" id="license-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add License State</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">State</label>
                                <select class="form-control" id="license-state">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block text-danger" style="display: none; color:red;">State field is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">License Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="license-expiry-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">Expiry Date is required.</span>
                            </div>
                        </div>

                        <div class="row"></div>
                        <div class="col-md-6">
                            <label for="">License Number</label>
                            <input type="text" id="license-number" class="form-control">
                            <span class="help-block text-danger" style="display: none; color:red;">License number is required.</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="license-comment">Comment</label><br>
                            <textarea id="license-comment" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="add-license-state">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Edit License state Modal--}}
    <div class="modal fade in" id="edit-license-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit License State</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="edit-tracker-license" value="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">State</label>
                                <select class="form-control" id="edit-license-state">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block text-danger" style="display: none; color:red;">State field is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">License Expiry Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="edit-license-expiry-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">Expiry Date is required.</span>
                            </div>
                        </div>

                        <div class="row"></div>
                        <div class="col-md-6">
                            <label for="">License Number</label>
                            <input type="text" id="edit-license-number" class="form-control">
                            <span class="help-block text-danger" style="display: none; color:red;">License number is required.</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label for="license-comment">Comment</label><br>
                            <textarea id="edit-license-comment" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="edit-license-state-done">Edit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- Inactive Reason Date Model --}}
    <div class="modal fade in" id="inactive-reason-date-modal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Inactive Dates</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Start Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="start-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">Start Date is required.</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label for="">End Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="end-date" class="form-control datepicker">
                                <span class="help-block text-danger" style="display: none; color:red;">End Date is required.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-inactive-reason-date">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataType').change(function () {
                var data_type = $(this).val();
                switch (data_type) {
                    case 'field':
                        $('.fieldDataInputs').fadeIn('slow');
                        $('.staffDataInputs').fadeOut('slow');
                        break;
                    case 'staff':
                        $('.fieldDataInputs').fadeOut('slow');
                        $('.staffDataInputs').fadeIn('slow');
                        break;
                }
            });

            $('.select2').select2();
            var x = 1;
            var html = '<div class="row">\n' +
                '                        <div class="col-md-5">\n' +
                '                            <div class="form-group">\n' +
                '                                <label for="">Certificate</label>\n' +
                '                                <input type="text" class="form-control certificates" name="certificates">\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '\n' +
                '                        <div class="col-md-5">\n' +
                '                                <label for="">Expiry Date</label>\n' +
                '                            <div class="input-group">\n' +
                '                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>\n' +
                '                           <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">\n' +
                '                        </div> </div>\n' +
                '\n' +
                '                        <div class="col-md-2" style="margin-top:30px;">\n' +
                '                            <button class="btn btn-danger remove-field" type="button"><i class="fa fa-minus"></i></button>\n' +
                '                        </div>\n' +
                '                    </div>';

            $("#add-more").click(function () {
                if (x < 11) {
                    $("#certificate-wrapper").append(html);
                    x++;
                    $('.datepicker').datepicker({
                        autoclose: true,
                        format: 'mm-dd-yyyy'
                    });
                }
            });

            $("#certificate-wrapper").on('click', '.remove-field', function () {
                $(this).closest('div .row').remove();
                x--;
            });

            //add certificate
            $("#add-certificate").click(function () {
                var certificate_html = '';
                var certificate_input_html = '';
                var certificate_names = [];
                var expiry_dates = [];
                $(".certificates").each(function () {
                    if ($(this).val() != '') {
                        certificate_names.push($(this).val());
                    }
                });

                $('.expiry-dates').each(function () {
                    if ($(this).val() != '') {
                        expiry_dates.push($(this).val());
                    }
                });

                if (certificate_names.length > 0) {
                    $(certificate_names).each(function (i, d) {
                        certificate_html += '<div class="single-container" id="single-container-' + i + 1 + '"><input type="radio" name="single-certificated" class="single-certificate" value="' + i + 1 + '">&nbsp;' + d + ' | ' + expiry_dates[i] + '</div>';
                        certificate_input_html += '<div id="certificate-hidden-input-set-' + i + 1 + '"><input type="hidden" class="certificate-name" name="certificate_names[]" value="' + d + '">';
                        certificate_input_html += '<input type="hidden" class="expiry-date" name="certificate_expiry_dates[]" value="' + expiry_dates[i] + '"></div>';
                    });

                    $("#certificate-container").append(certificate_html);
                    $("#certificate-hidden-input").append(certificate_input_html);

                    //change the modal input to default
                    $("#certificate-wrapper").html('<div class="row">\n' +
                        '                        <div class="col-md-5">\n' +
                        '                            <div class="form-group">\n' +
                        '                                <label for="">Certification</label>\n' +
                        '                                <input type="text" class="form-control certificates" name="certificates">\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="col-md-5">\n' +
                        '                            <label for="">Expiry Date</label>\n' +
                        '                            <div class="input-group">\n' +
                        '                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>\n' +
                        '                                <input type="text" class="form-control expiry-dates datepicker" name="expiry_dates">\n' +
                        '\n' +
                        '                            </div>\n' +
                        '                        </div>\n' +
                        '\n' +
                        '                        <div class="col-md-2" style="margin-top:30px;">\n' +
                        '                            <button class="btn btn-primary" id="add-more"><i class="fa fa-plus"></i></button>\n' +
                        '                        </div>\n' +
                        '                    </div>');

                    //closing the modal
                    $("#mymodal").modal('hide');

                    // //init the datepicker class again
                    $('.datepicker').datepicker({
                        autoclose: true,
                        format: 'mm-dd-yyyy'
                    })
                } else {
                    alert('Please fill at-least one record');
                }
            });
        });

        //setting the selected certificate details in edit inputs
        $("#edit-certificate").click(function () {
            if ($(".single-certificate:checked").length > 0) {
                $("#edit-certificate-modal").modal('show');
                var need_to_be_edit = $(".single-certificate:checked").val();
                var certificate_name_input = $($("#certificate-hidden-input-set-" + need_to_be_edit + " > :input")[0]).val();
                var expiry_date_input = $($("#certificate-hidden-input-set-" + need_to_be_edit + " > :input")[1]).val();
                $("#edit-certificate-input").val(certificate_name_input);
                $("#edit-expiry-date-input").val(expiry_date_input);
                $("#edit-tracker").val(need_to_be_edit);
            } else {
                alert('Please select the certificate.');
            }
        });

        $("#edit-certificate-done").click(function () {
            var certificate_name = $("#edit-certificate-input").val();
            var expiry_date = $("#edit-expiry-date-input").val();
            var edit_tacker = $("#edit-tracker").val();

            certificate_input_html = '<input type="hidden" class="certificate-name" name="certificate_names[]" value="' + certificate_name + '">';
            certificate_input_html += '<input type="hidden" class="expiry-date" name="certificate_expiry_dates[]" value="' + expiry_date + '">';
            $("#certificate-hidden-input-set-" + edit_tacker).html(certificate_input_html);
            $("#single-container-" + edit_tacker).html('<input type="radio" class="single-certificate" value="' + edit_tacker + '">&nbsp;' + certificate_name + ' | ' + expiry_date);
            $("#edit-certificate-modal").modal('hide');
        });

        //delete the specific single certificate
        $("#delete-certificate").click(function () {
            if ($(".single-certificate:checked").length > 0) {
                var need_to_be_delete = $(".single-certificate:checked").val();
                $("#certificate-hidden-input-set-" + need_to_be_delete).remove();
                $("#single-container-" + need_to_be_delete).remove();
            } else {
                alert('Please select the certificate.');
            }
        });

        //-- Add State License
        $("#add-license-state").click(function () {
            var state = $("#license-state");
            var state_name = $('#license-state option:selected').text();
            var expiry_date = $("#license-expiry-date");
            var license_number = $("#license-number");
            var license_comment = $("#license-comment");
            if (state.val().length == 0) {
                state.next('span').fadeIn(200);
            } else if (expiry_date.val().length == 0) {
                expiry_date.next('span').fadeIn(200);
            } else if (license_number.val().length == 0) {
                license_number.next('span').fadeIn(200);
            } else {
                var html = '';
                var license_radio = '';
                var license_tracker = localStorage.getItem('license_tracker') ? (parseInt(localStorage.getItem('license_tracker')) + 1) : 1;
                html = '<div id="license-' + license_tracker + '">';
                html += "<input type='hidden' name='license_states[]' value='" + state.val() + "'>";
                html += "<input type='hidden' name='license_expiry_dates[]' value='" + expiry_date.val() + "'>";
                html += "<input type='hidden' name='license_numbers[]' value='" + license_number.val() + "'>";
                html += "<input type='hidden' name='license_comments[]' value='" + license_comment.val() + "'>";
                html += "</div>";

                //making the license radio input
                license_radio = '<div class="single-container" id="single-license-' + license_tracker + '"><input type="radio" name="single-license" class="single-license" value="' + license_tracker + '">&nbsp;' + state_name + ' | ' + license_number.val() + ' | ' + expiry_date.val() + '</div>';
                $("#license-home-states").append(html);
                $("#license-container").append(license_radio);

                //storing license tracker in browser cache
                localStorage.setItem('license_tracker', license_tracker);

                //clearing the inputs
                state.prop('selectedIndex', 0);
                expiry_date.val('00/00/0000');
                license_number.val('');
                license_comment.val('');

                //closing the modal
                $("#license-modal").modal('hide');
            }
        });

        //-- Edit State License
        $("#edit-license-state-btn").click(function () {
            if ($(".single-license:checked").length > 0) {
                $("#edit-license-modal").modal('show');
                var need_to_be_edit = $(".single-license").val();
                var state = $($("#license-" + need_to_be_edit + "> input")[0]).val();
                var expiry_date = $($("#license-" + need_to_be_edit + "> input")[1]).val();
                var license_number = $($("#license-" + need_to_be_edit + "> input")[2]).val();
                var license_comment = $($("#license-" + need_to_be_edit + "> input")[3]).val();

                // //maintain the edit license tracker
                // $("#edit-license-tracker").val(need_to_be_edit);

                //assign value to modal inputs
                $("#edit-license-state option").each(function () {
                    if ($(this).val() == state) {
                        $(this).attr('selected', true);
                    }
                });
                $("#edit-license-expiry-date").val(expiry_date);
                $("#edit-license-number").val(license_number);
                $("#edit-license-comment").val(license_comment);
            } else {
                alert('Please select the license state.')
            }
        });

        //-- Done Edit State License
        $("#edit-license-state-done").click(function () {
            var state = $("#edit-license-state");
            var state_name = $('#edit-license-state option:selected').text();
            var expiry_date = $("#edit-license-expiry-date");
            var license_number = $("#edit-license-number");
            var license_comment = $("#edit-license-comment");
            if (state.val().length == 0) {
                state.next('span').fadeIn(200);
            } else if (expiry_date.val().length == 0) {
                expiry_date.next('span').fadeIn(200);
            } else if (license_number.val().length == 0) {
                license_number.next('span').fadeIn(200);
            } else {
                var html = '';
                var license_radio = '';
                var license_tracker = localStorage.getItem('license_tracker');
                html += "<input type='hidden' name='license_states[]' value='" + state.val() + "'>";
                html += "<input type='hidden' name='license_expiry_dates[]' value='" + expiry_date.val() + "'>";
                html += "<input type='hidden' name='license_numbers[]' value='" + license_number.val() + "'>";
                html += "<input type='hidden' name='license_comments[]' value='" + license_comment.val() + "'>";

                //making the license radio input
                license_radio = '<input type="radio" class="single-license" value="' + license_tracker + '">' + state_name + ' - ' + license_number.val();
                $("#license-" + license_tracker).html(html);
                $("#single-license-" + license_tracker).html(license_radio);


                //clearing the inputs
                state.prop('selectedIndex', 0);
                expiry_date.val('00/00/0000');
                license_number.val('');
                license_comment.val('');

                //closing the modal
                $("#edit-license-modal").modal('hide');
            }
        });

        //delete the specific single certificate
        $("#delete-license-state").click(function () {
            if ($(".single-license:checked").length > 0) {
                var need_to_be_delete = $(".single-license:checked").val();
                alert(need_to_be_delete);
                $("#single-license-" + need_to_be_delete).remove();
                $("#license-" + need_to_be_delete).remove();
            } else {
                alert('Please select the certificate.');
            }
        });

        $("#inactive-reason").change(function () {
            if ($(this).val() != '') {
                //open modal
                $('#inactive-reason-date-modal').modal('show');
            }
        });

        $("#save-inactive-reason-date").click(function () {
            var start_date = $("#start-date");
            var end_date = $("#end-date");
            if (start_date.val().length == 0) {
                start_date.next('span').fadeIn(200);
            } else if (end_date.val().length == 0) {
                end_date.next('span').fadeIn(200);
            } else {
                $("#start-date-hidden").val(start_date.val());
                $("#end-date-hidden").val(end_date.val());

                //close the modal
                $("#inactive-reason-date-modal").modal('hide');
            }
        });

        $("#all_states").change(function () {
            var state = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{ route('get.cities') }}",
                data: {state_id: state, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        var options = '';
                        options += '<option value="">--Select--</option>';
                        response.cities.forEach(function (data) {
                            options += '<option value="' + data.id + '">' + data.name + '</option>';
                        });

                        $("#cities").html(options);
                    }
                }
            })
        });

        //USA Phone no. Mask
        $('[data-mask]').inputmask();

        $(".datepicker").datepicker({
            format: 'mm-dd-yyyy'
        })

        function checkDate() {
            if ($("#flagged").is(':checked')) {
                $("#flag_check_date").removeAttr('hidden');
            } else {
                $("#flag_check_date").attr('hidden', 'hidden');
            }

        }

        var tax_id = $('#tax_id').val();
        // $('#tax_id').on('focusout', function () {
        //     var value = $(this).val();
        //     var replaced = value.replace(/.(?=.{3,}$)/g, '*');
        //     $(this).val(replaced);
        // })
    </script>
@endpush
