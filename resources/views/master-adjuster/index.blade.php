@extends('adminlte::page')
@section('title', 'Data Form')

@push('css')
<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></link>    
<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"></link>
<style>
    .dt-buttons {
        float : right;
        margin: -40px 16px -35px 0px; 
    }
    .dataTables_filter {        
        margin-top: 30px; 
    }
</style>
@endpush

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Adjusters</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('adjusters.create') }}" class="btn btn-primary btnTheme">Add Data Form</a>
            </div>
        </div>
        
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Id</th>
                   @if($form_fields->first_name==true) <th>Name</th>@endif
                   @if($form_fields->state==true) <th>State</th>@endif
                   @if($form_fields->assign_type_id==true)<th>Type</th>@endif
                   @if($form_fields->city_id==true) <th>City</th>@endif
                   @if($form_fields->primary_phone_no==true) <th>Phone No.</th>@endif
                   @if($form_fields->email==true) <th>Email</th>@endif
                   @if($form_fields->initial_service_date==true) <th>Initial Service Date</th>@endif
                   @if($form_fields->dob==true) <th>DOB</th>@endif
                   @if($form_fields->secondary_phone_no==true) <th>Secondary Phone No</th>@endif
                   @if($form_fields->emergency_contact_name==true) <th>Emergency Contact Name</th>@endif
                   @if($form_fields->emergency_phone_no==true) <th>Emergency Phone No</th>@endif
                   @if($form_fields->street_number==true) <th>Street Number</th>@endif
                   @if($form_fields->zip==true) <th>Zip</th>@endif
                   @if($form_fields->driver_license==true) <th>Driver License</th>@endif
                   @if($form_fields->dl_state==true) <th>Dl State</th>@endif
                   @if($form_fields->tx_id==true) <th>Tx Id</th>@endif
                   @if($form_fields->license_name==true) <th>License Name</th>@endif
                   @if($form_fields->company_tin==true) <th>Company Tin</th>@endif
                   @if($form_fields->company_llc_name==true) <th>Company llc Name</th>@endif
                   @if($form_fields->state_license==true) <th>State License</th>@endif
                   @if($form_fields->speciality==true) <th>Speciality</th>@endif
                   @if($form_fields->personal_folder_made==true) <th>Personal Folder Made</th>@endif
                   @if($form_fields->fluent_languages==true) <th>Fluent Languages</th>@endif
                   @if($form_fields->npn_number==true) <th> Npn Number</th>@endif
                   @if($form_fields->check_background_date==true) <th>Background Check Date</th>@endif
                   @if($form_fields->is_flagged==true) <th>Flagged</th>@endif
                   @if($form_fields->comment==true) <th>Comment</th>@endif
                   @if($form_fields->url==true) <th>url</th>@endif
                    <th>Total Open</th>
                </tr>
                </thead>
                <tbody>
                    @if(!empty($adjusters))
                        @foreach($adjusters as $adjuster)
                            <tr>
                                <td>
                                    <a href="{{ route('adjusters.edit', $adjuster->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                            <a href="{{ route('adjusters.show', $adjuster->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                            <form action="#" method="POST" class="delete_item">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                            </form>
                                    </td>
                            <td>{{ $loop->iteration }}</td>
                            @if($form_fields->first_name==true) <td>{{ $adjuster->getFullName() }}</td> @endif
                            @if($form_fields->state==true) <td>{{ $adjuster->state->name ?? '--' }}</td> @endif
                            @if($form_fields->assign_type_id==true)<td>
                             @if(!empty($adjuster->types))
                                @foreach($adjuster->types as $key => $type)
                                        {{ $type->name }}
                                        @if($key >= 0 && $key < count($adjuster->types) - 1)
                                            ,
                                        @endif
                                @endforeach
                            @endif
                            </td>@endif
                            @if($form_fields->city_id==true) <td>{{ $adjuster->city->name ?? '--' }}</td> @endif
                            @if($form_fields->primary_phone_no==true) <td>{{ $adjuster->primary_phone_no ?? '--' }}</td> @endif
                            @if($form_fields->email==true) <td>{{ $adjuster->email ?? '--' }}</td> @endif
                            @if($form_fields->initial_service_date==true) <td>{{ $adjuster->initial_service_date ?? '--' }}</td> @endif
                            @if($form_fields->dob==true) <td>{{ $adjuster->dob ?? '--' }}</td> @endif
                            @if($form_fields->secondary_phone_no==true) <td>{{ $adjuster->secondary_phone_no ?? '--' }}</td> @endif
                            @if($form_fields->emergency_contact_name==true) <td>{{ $adjuster->emergency_contact_name ?? '--' }}</td> @endif
                            @if($form_fields->emergency_phone_no==true) <td>{{ $adjuster->emergency_phone_no ?? '--' }}</td> @endif
                            @if($form_fields->street_number==true) <td>{{ $adjuster->street_number ?? '--' }}</td> @endif
                            @if($form_fields->zip==true) <td>{{ $adjuster->zip ?? '--' }}</td> @endif
                            @if($form_fields->driver_license==true) <td>{{ $adjuster->driver_license ?? '--' }}</td> @endif
                            @if($form_fields->dl_state==true) <td>{{ $adjuster->dl_state ?? '--' }}</td> @endif
                            @if($form_fields->tx_id==true) <td>{{ $adjuster->tx_id ?? '--' }}</td> @endif
                            @if($form_fields->license_name==true) <td>{{ $adjuster->license_name ?? '--' }}</td> @endif
                            @if($form_fields->company_tin==true) <td>{{ $adjuster->company_tin ?? '--' }}</td> @endif
                            @if($form_fields->company_llc_name==true) <td>{{ $adjuster->company_llc_name ?? '--' }}</td> @endif
                            @if($form_fields->state_license==true) <td>{{ $adjuster->state_license ?? '--' }}</td> @endif
                            @if($form_fields->speciality==true) <td>{{ $adjuster->speciality ?? '--' }}</td> @endif
                            @if($form_fields->personal_folder_made==true) <td>{{ $adjuster->personal_folder_made ?? '--' }}</td> @endif
                            @if($form_fields->fluent_languages==true) 
                            <td>
                                @if ($adjuster->fluent_languages!=null)
                                @foreach (json_decode($adjuster->fluent_languages) as $fluent_language)
                                {{ $fluent_language ?? '--' }}
                                @endforeach
                                @else
                                @endif
                            </td> 
                            @endif
                            @if($form_fields->npn_number==true) <td>{{ $adjuster->npn_number ?? '--' }}</td> @endif
                            @if($form_fields->check_background_date==true) <td>{{ $adjuster->check_background_date ?? '--' }}</td> @endif
                            @if($form_fields->is_flagged==true) <td>{{ $adjuster->is_flagged ? 'Yes' : 'No' }}</td> @endif
                            @if($form_fields->comment==true) <td>{{ $adjuster->comment ?? '--' }}</td> @endif
                            @if($form_fields->url==true) <td>{{ $adjuster->url=="www."?'':$adjuster->url }}</td> @endif

                            <td>{{ $adjuster->log_assignments->count() }}</td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
<script src= "https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src= "https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src= "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src= "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src= "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src= "https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src= "https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'dom': 'Bfrtip',
                'lengthChange': true,
                'pagelength': 10,
                "scrollX": true,
                "columnDefs": [
                 // { "width": "15%", "targets": 1 }
                ],
                'buttons': [
                    {
                    'extend': 'csvHtml5',
                    'text'  : 'Download CSV',
                    'className': 'btn btn-primary btnTheme',
                    'exportOptions': {
                    'columns': ':not(:first-child)'
                  }
                }

                ]
            });
        });
    </script>
@endpush
