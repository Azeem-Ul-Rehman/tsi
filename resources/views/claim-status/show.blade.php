@extends('adminlte::page')
@section('title', 'Adjuster Log Assignment')

@section('content')
<div class="box">
            <div class="box-header customHeader">
                <h3 class="box-title">{{ $claim_status->master_adjuster->getFullName() }} - {{ $claim_status->master_adjuster->state->name }}</h3>
                <a href="{{ route('adjusters.edit', $claim_status->master_adjuster->id) }}" style="float:right; color:white;">Edit</a>
            </div>
            <div class="col-md-12" style="padding: 10px;">
                {{-- <div class="col-md-1"></div> --}}
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('adjusters.show', $claim_status->master_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.homestate', $claim_status->master_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.corrections', $claim_status->master_adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.rejections', $claim_status->master_adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.log-assignments', $claim_status->master_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
                <a href="{{ route('tab.adjuster.claim-status', $claim_status->master_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Claim Status</a>

            </div>
            <div class="clearfix"></div>
            <div class="box-body table-responsive no-padding">
                <div class="box-header customHeader">
                    <h3 class="box-title"><b>Claim Number: {{ $claim_status->claim_number}}</b></h3>
                    <a href="{{ route('claim-status.edit', $claim_status->id) }}" style="float:right; color:white;">Edit</a>
                </div>
              <table class="table table-bordered tableStyle">
               <tbody>
               
                <tr>
                    <th>TSI Number</th>
                    <td>{{ $claim_status->tsi_number ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Received</th>
                    <td>{{ $claim_status->date_received ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Adjuster</th>
                    <td>{{ $claim_status->master_adjuster->getFullName() ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Insured</th>
                    <td>{{ $claim_status->insured ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Company</th>
                    <td>{{ $claim_status->company ?? '-'}}</td>
                </tr>
                <tr>
                    <th>Claim Number</th>
                    <td>{{ $claim_status->claim_number ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date of notice</th>
                    <td>{{ $claim_status->date_of_notice ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Contacted</th>
                    <td>{{ $claim_status->date_contacted ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Inspected</th>
                    <td>{{ $claim_status->date_inspected ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Report Due</th>
                    <td>{{ $claim_status->report_due ?? "-" }}</td>
                </tr>
                 <tr>
                    <th>Note</th>
                    <td>{{ $claim_status->note ?? "-" }}</td>
                </tr>
            
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection

@push('css')
<style type="text/css">
.tableStyle th
{
    width: 20%;
}
</style>
@endpush
