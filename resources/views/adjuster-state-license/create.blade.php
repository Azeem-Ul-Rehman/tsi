@extends('adminlte::page')
@section('title', 'Adjuster Home State License Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">Adjuster Home State License Form</h3>
        </div>
        <form action="{{ route('home-state-license.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="stateForm">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('adjuster') ? 'has-error': '' }}">
                                    <label>Select Adjuster</label>
                                    <select name="adjuster" class="form-control license-states">
                                        @if(!empty($adjusters))
                                            @foreach($adjusters as $adjuster)
                                                <option
                                                        value="{{ $adjuster->id }}">{{ $adjuster->getFullName() }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if($errors->has('license_state'))
                                        <span
                                                class="help-block text-danger">{{ $errors->first('license_state') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('npn_number') ? 'has-error': '' }}">
                                            <label>NPN Number</label>
                                            <input type="text" name="npn_number" class="form-control"
                                                   value="">
                                            @if($errors->has('npn_number'))
                                                <span
                                                        class="help-block text-danger">{{ $errors->first('npn_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('expiry_date') ? 'has-error': '' }}">
                                            <label>License Expiry Date</label>
                                            <input type="text" name="expiry_date" class="form-control datepicker"
                                                   value="">
                                            @if($errors->has('expiry_date'))
                                                <span
                                                        class="help-block text-danger">{{ $errors->first('expiry_date') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('license_state') ? 'has-error': '' }}">
                                            <label>License State</label>
                                            <select name="license_state" class="form-control license-states">
                                                @if(!empty($states))
                                                    @foreach($states as $state)
                                                        <option
                                                                value="{{ $state->id }}">{{ $state->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if($errors->has('license_state'))
                                                <span
                                                        class="help-block text-danger">{{ $errors->first('license_state') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div
                                                class="form-group {{ $errors->has('license_number') ? 'has-error': '' }}">
                                            <label>License Number</label>
                                            <input type="text" name="license_number" class="form-control"
                                                   value="">
                                            @if($errors->has('license_number'))
                                                <span
                                                        class="help-block text-danger">{{ $errors->first('license_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="display: block;">&nbsp;</label>
                                            <label><input type="checkbox" class="home-states" name="home_state"
                                                          value="1"><b> Home
                                                    State</b> </label>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label style="display: block;">&nbsp;</label>
                                            <label><input type="radio" name="active" value="1" checked>
                                                Active </label> <br>
                                            <label><input type="radio" name="active" value="0">
                                                InActive </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <textarea class="form-control" rows="5"
                                              name="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btnTheme">Submit</button>
                </div>
        </form>
    </div>
@endsection
