@extends('adminlte::page')
@section('title', 'Adjuster Home State License Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">Edit State License</h3>
        </div>
        <div class="box-body">
            <form action="{{ route('home-state-license.update', $license->id) }}" method="POST">
                @csrf
                {{ method_field("PUT") }}
                <input type="hidden" name="adjuster_id" value="{{ $license->adjuster->id }}"/>
                <div class="stateForm">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('npn_number') ? 'has-error': '' }}">
                                        <label>NPN Number</label>
                                        <input type="text" name="npn_number" class="form-control"
                                               value="{{ $license->npn_number }}">
                                        @if($errors->has('npn_number'))
                                            <span
                                                    class="help-block text-danger">{{ $errors->first('npn_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('expiry_date') ? 'has-error': '' }}">
                                        <label>License Expiry Date</label>
                                        <input type="text" name="expiry_date" class="form-control datepicker"
                                               value="{{ $license->expiry_date }}">
                                        @if($errors->has('expiry_date'))
                                            <span
                                                    class="help-block text-danger">{{ $errors->first('expiry_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group {{ $errors->has('license_state') ? 'has-error': '' }}">
                                        <label>License State</label>
                                        <select name="license_state" class="form-control license-states">
                                            @if(!empty($states))
                                                @foreach($states as $state)
                                                    <option
                                                            value="{{ $state->id }}" {{ ($license->state_id == $state->id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if($errors->has('license_state'))
                                            <span
                                                    class="help-block text-danger">{{ $errors->first('license_state') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div
                                            class="form-group {{ $errors->has('license_number') ? 'has-error': '' }}">
                                        <label>License Number</label>
                                        <input type="text" name="license_number" class="form-control"
                                               value="{{ $license->license_number }}">
                                        @if($errors->has('license_number'))
                                            <span
                                                    class="help-block text-danger">{{ $errors->first('license_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="display: block;">&nbsp;</label>
                                        <label><input type="checkbox" class="home-states" name="home_state"
                                                      {{ ($license->home_state == 1) ? 'checked' : ''  }} value="1"><b> Home
                                                State</b> </label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label style="display: block;">&nbsp;</label>
                                        <label><input type="radio" name="active"
                                                      {{ ($license->active == 1) ? 'checked' : ''  }} value="1">
                                            Active </label> <br>
                                        <label><input type="radio" name="active"
                                                      {{ ($license->active == 0) ? 'checked' : ''  }} value="0">
                                            InActive </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="">Comment</label>
                                <textarea class="form-control" rows="5"
                                          name="comment">{{ $license->comment }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label style="display: block; margin-top: 90px;">&nbsp;</label>
                            <button class="btn btn-success" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(".home-states").click(function () {
            var data_id = $(this).data('id');
            $(".home-states").each(function () {
                if ($(this).data('id') != data_id) {
                    $(this).prop('checked', false);
                } else {
                    $(this).attr('checked', true);
                }
            });
        });
    </script>
@endpush
