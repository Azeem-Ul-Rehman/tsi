@extends('adminlte::page')
@section('title', 'Adjuster Home State License Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title"><b>{{ $adjuster->getFullName() ?? "-" }} - {{ $adjuster->state->name ?? "-" }}</b></h3>
            <a href="{{ route('adjusters.show', $adjuster->id) }}" style="float:right; color:white;">View</a>
        </div>
        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adjuster->id) }}" class="btn btn-primary btnTheme" >Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adjuster->id) }}" class="btn btn-primary btnTheme" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;">State Licenses</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.corrections', $adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.log-assignments', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

        </div>
        <div class="clearfix"></div>
        <div class="box-body">
            <div class="box-header customHeader">
                <h3 class="box-title"><b>State License</b></h3>
            </div>
            @if(!empty($adjuster))
                @foreach($adjuster->home_state_licenses as $license)
                    <form action="{{ route('home-state-license.update', $license->id) }}" method="POST">
                        @csrf
                        {{ method_field("PUT") }}
                        <input type="hidden" name="adjuster_id" value="{{ $adjuster->id }}"/>
                        <div class="stateForm">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('first_name') ? 'has-error': '' }}">
                                                <label>First Name</label>
                                                <input type="text" name="first_name" class="form-control"
                                                       value="{{ $adjuster->first_name }}" required="">
                                                @if($errors->has('first_name'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('last_name') ? 'has-error': '' }}">
                                                <label>Last Name</label>
                                                <input type="text" name="last_name" class="form-control"
                                                       value="{{ $adjuster->last_name }}" required="">
                                                @if($errors->has('last_name'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('dob') ? 'has-error': '' }}">
                                                <label>Date of Birth</label>
                                                <input type="text" name="dob" class="form-control datepicker"
                                                       value="{{ $adjuster->dob }}" required="">
                                                @if($errors->has('dob'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('dob') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                                                <label>City</label>
                                                <select name="city" class="form-control">
                                                    @if(!empty($cities))
                                                        @foreach($cities as $city)
                                                            <option value="{{ $city->id }}" {{ ($city->id == $adjuster->city_id) ? 'selected' : '' }}>{{ $city->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                @if($errors->has('city'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                                                <label>State</label>
                                                <select class="form-control states" name="state">
                                                    @if(!empty($states))
                                                        @foreach($states as $state)
                                                            <option
                                                                value="{{ $state->name }}" {{ ($adjuster->state == $state->name) ? 'selected' : '' }}>{{ $state->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if($errors->has('state'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('state') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('npn_number') ? 'has-error': '' }}">
                                                <label>NPN Number</label>
                                                <input type="text" name="npn_number" class="form-control"
                                                       value="{{ $license->npn_number }}">
                                                @if($errors->has('npn_number'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('npn_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('expiry_date') ? 'has-error': '' }}">
                                                <label>License Expiry Date</label>
                                                <input type="text" name="expiry_date" class="form-control datepicker"
                                                       value="{{ $license->expiry_date }}">
                                                @if($errors->has('expiry_date'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('expiry_date') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('license_state') ? 'has-error': '' }}">
                                                <label>License State</label>
                                                <select name="license_state" class="form-control license-states">
                                                    @if(!empty($states))
                                                        @foreach($states as $state)
                                                            <option
                                                                value="{{ $state->id }}" {{ ($license->state_id == $state->id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if($errors->has('license_state'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('license_state') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div
                                                class="form-group {{ $errors->has('license_number') ? 'has-error': '' }}">
                                                <label>License Number</label>
                                                <input type="text" name="license_number" class="form-control"
                                                       value="{{ $license->license_number }}">
                                                @if($errors->has('license_number'))
                                                    <span
                                                        class="help-block text-danger">{{ $errors->first('license_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label style="display: block;">&nbsp;</label>
                                                <label><input type="checkbox" class="home-states"
                                                              data-id="home-state-{{$loop->iteration}}"
                                                              name="home_state"
                                                              {{ ($license->home_state == 1) ? 'checked' : ''  }} value="1"><b> Home
                                                        State</b> </label>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label style="display: block;">&nbsp;</label>
                                                <label><input type="radio" name="active"
                                                              {{ ($license->active == 1) ? 'checked' : ''  }} value="1">
                                                    Active </label> <br>
                                                <label><input type="radio" name="active"
                                                              {{ ($license->active == 0) ? 'checked' : ''  }} value="0">
                                                    InActive </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="">Comment</label>
                                        <textarea class="form-control" rows="5"
                                                  name="comment">{{ $license->comment }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label style="display: block; margin-top: 90px;">&nbsp;</label>
                                    <button class="btn btn-success" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(".home-states").click(function () {
            var data_id = $(this).data('id');
            $(".home-states").each(function () {
                if ($(this).data('id') != data_id) {
                    $(this).prop('checked', false);
                } else {
                    $(this).attr('checked', true);
                }
            });
        });
    </script>
@endpush
