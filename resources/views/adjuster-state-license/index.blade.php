@extends('adminlte::page')
@section('title', 'Home State License')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Adjuster State License</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('home-state-license.create') }}" class="btn btn-primary btnTheme">Create State License</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Total License</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($adjusters))
                    @foreach($adjusters as $adjuster)
                        <tr>
                            <td>{{ $adjuster->id }}</td>
                            <td>{{ $adjuster->getFullName() }}</td>
                            <td>
                                @if(!empty($adjuster->types))
                                    @foreach($adjuster->types as $key => $type)
                                        {{ $type->name }}
                                        @if($key >= 0 && $key < count($adjuster->types) - 1)
                                            ,
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $adjuster->home_state_licenses()->count() }}</td>
                            <td>
                                <a href="{{ route('home-state-license.edit', $adjuster->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="{{ route('home-state-license.show', $adjuster->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="{{ route('home-state-license.destroy', $adjuster->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
