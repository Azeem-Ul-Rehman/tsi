@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title">Search Adjuster</h3>
            </div>
            
            <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-2">
                    <label for="">First Name</label>
                    <input type="text" id="first_name" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="">Last Name</label>
                    <input type="text" id="last_name" class="form-control">
                </div>
                <div class="col-md-2">
                    <label for="">Roles</label>
                    <select name="" id="role" class="form-control">
                        <option value="">--Select--</option>
                        @if(!empty($roles))
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="col-md-2">
                    <label for="">States</label>
                    <select name="" id="state" class="form-control">
                        <option value="">--Select--</option>
                        @if(!empty($states))
                            @foreach($states as $state)
                                <option value="{{ $state->name }}">{{ $state->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="col-md-2">
                    <label for="">Types</label>
                    <select name="" id="type" class="form-control">
                        <option value="">--Select--</option>
                        @if(!empty($types))
                            @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="col-md-2">
                    <label for="">Grades</label>
                    <select name="" id="grade" class="form-control">
                        <option value="">--Select--</option>
                        @if(!empty($grades))
                            @foreach($grades as $grade)
                                <option value="{{ $grade->id }}">{{ $grade->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-2">
                    <label for="">Driver License</label>
                    <input type="text" id="driver-license" class="form-control">
                </div>

                <div class="col-md-2">
                    <label for="">State License</label>
                    <select class="form-control" id="state-license">
                        <option value="">--Select--</option>
                        @if(!empty($states))
                            @foreach($states as $state)
                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="col-md-2">
                    <label for="">City</label>
                    <select class="form-control" id="city">
                        <option value="">--Select--</option>
                        @if(!empty($cities))
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="col-md-2">
                    <label for="">Inactive Reason</label>
                    <select class="form-control" id="inactive-reason">
                        <option value="">--Select--</option>
                        @if(!empty($inactive_reasons))
                            @foreach($inactive_reasons as $inactive_reason)
                                <option value="{{ $inactive_reason->id }}">{{ $inactive_reason->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="col-md-12 text-center" style="margin-top:10px;">
                <button class="btn btn-primary" id="search-adjuster">Search</button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>State</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="filter-rows">
                @if(!empty($adjusters))
                    @foreach($adjusters as $adjuster)
                        <tr>
                            <td>{{ $adjuster->id }}</td>
                            <td>{{ $adjuster->getFullName() }}</td>
                            <td>{{ $adjuster->secondary_service_area->name??'--' }}</td>
                            <td>
                                @if(!empty($adjuster->types))
                                    @foreach($adjuster->types as $key => $type)
                                        {{ $type->name }}
                                        @if($key >= 0 && $key < count($adjuster->types) - 1)
                                            ,
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('adjusters.edit', $adjuster->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="#"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="#" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
                'searching': false
            });
        });

        $("#search-adjuster").click( function(){
           var role = $("#role").val();
           var grade = $("#grade").val();
           var type = $("#type").val();
           var state = $("#state").val();
           var driver_license = $("#driver-license").val();
           var first_name = $("#first_name").val();
           var last_name = $("#last_name").val();
           var state_license = $("#state-license").val();
           var inactive_reason = $("#inactive-reason").val();
           var city = $("#city").val();

           $.ajax({
               url: "{{ route('search.adjuster') }}",
               method: 'POST',
               data: {_token: '{{csrf_token()}}', first_name: first_name, last_name: last_name, driver_licenese: driver_license, role: role, grade: grade, type: type, state: state, state_license: state_license, inactive_reason: inactive_reason, city: city},
               cache: false,
               success: function(response){
                   console.log(response);
                   if(typeof response == 'string'){
                       toastr['success']('Record has been filters');
                       $("#filter-rows").html(response);
                   }else{
                       toastr['error'](response.message);
                   }
               },
               error: function(){
                    toastr['error']('Something went wrong');
                    $("#filter-rows").empty();
               }
           });
        });
    </script>
@endpush
