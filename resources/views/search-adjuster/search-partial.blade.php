@if(!empty($filter_adjusters))
    @foreach($filter_adjusters as $adjuster)
        <tr>
            <td>{{ $adjuster->id }}</td>
            <td>{{ $adjuster->getFullName() }}</td>
            <td>{{ $adjuster->state ??'--' }}</td>
            <td>
                @if(!empty($adjuster->types))
                    @foreach($adjuster->types as $key => $type)
                        {{ $type->name }}
                        @if($key >= 0 && $key < count($adjuster->types) - 1)
                            ,
                        @endif
                    @endforeach
                @endif
            </td>
            <td>
                <a href="{{ route('adjusters.edit', $adjuster->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                <a href="{{ route('adjusters.show', $adjuster->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                <form action="#" method="POST" class="delete_item">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
@endif
