@extends('adminlte::page')
@section('title', 'Adjuster Log Assignment')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">TSI Log Assignment Form</h3>
        </div>
        <form action="{{ route('log-assignment.store') }}" method="POST">
            @csrf
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('claim_number') ? 'has-error': '' }}">
                            <label>Claim Number</label>
                            <input type="text" name="claim_number" class="form-control" value="{{ old('claim_number') }}">
                            @if($errors->has('claim_number'))
                                <span class="help-block text-danger">{{ $errors->first('claim_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('tsi_file_number') ? 'has-error': '' }}">
                            <label>TSI File Number</label>
                            <input type="text" name="tsi_file_number" class="form-control" value="{{ old('tsi_file_number') }}">
                            @if($errors->has('tsi_file_number'))
                                <span class="help-block text-danger">{{ $errors->first('tsi_file_number') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('adjuster') ? 'has-error': '' }}">
                            <label>Adjuster</label>
                            <select name="adjuster" class="form-control">
                                <option value="">--Select--</option>
                                @if(!empty($adjusters))
                                    @foreach($adjusters as $adjuster)
                                        <option value="{{ $adjuster->id }}" {{ (old('adjuster') == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('adjuster'))
                                <span class="help-block text-danger">{{ $errors->first('adjuster') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('insured') ? 'has-error': '' }}">
                            <label>Insured</label>
                            <input type="text" name="insured" class="form-control" value="{{ old('insured') }}">
                            @if($errors->has('insured'))
                                <span class="help-block text-danger">{{ $errors->first('insured') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                            <label>State</label>
                            <select name="state" class="form-control" id="all_states">
                                <option value="">--Select--</option>
                                @if(!empty($states))
                                    @foreach($states as $state)
                                        <option value="{{ $state->id }}" {{ (old('state') == $state->id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if($errors->has('state'))
                                <span class="help-block text-danger">{{ $errors->first('state') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                            <label>City</label>
                            <select name="city" id="cities" class="form-control">
                                <option value="">--Select--</option>
                            </select>
                            @if($errors->has('city'))
                                <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                            <label>Zip</label>
                            <input type="text" name="zip" class="form-control" value="{{ old('zip') }}">
                            @if($errors->has('zip'))
                                <span class="help-block text-danger">{{ $errors->first('zip') }}</span>
                            @endif
                        </div>
                    </div>
                
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('date') ? 'has-error': '' }}">
                        <label>Date</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="date" value="{{ old('date') }}">
                            @if($errors->has('date'))
                                <span class="help-block text-danger">{{ $errors->first('date') }}</span>
                            @endif
                        </div>
                    </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('status') ? 'has-error': '' }}">
                            <label>Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select</option>
                                <option value="open">Open</option>
                                <option value="close">Close</option>
                            </select>
                            @if($errors->has('status'))
                                <span class="help-block text-danger">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                    </div>
                </div>       
            </div>
            </div> 
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Save</button>
            </div>
        </form>
    </div>
@endsection

@push("js")
    <script>
         $("#all_states").change( function(){
            var state = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{ route('get.cities') }}",
                data: {state_id: state, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        var options = '';
                        options += '<option value="">--Select--</option>';
                        response.cities.forEach( function(data){
                            options += '<option value="'+ data.id +'">'+ data.name +'</option>';
                        });

                        $("#cities").html(options);
                    }
                }
            })
        });
    </script>
@endpush
