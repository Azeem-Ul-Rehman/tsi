@extends('adminlte::page')
@section('title', 'Adjuster Log Assignment')

@section('content')
<div class="box">
            <div class="box-header customHeader">
                <h3 class="box-title">{{ $log_assignment->assign_adjuster->getFullName() }} - {{ $log_assignment->assign_adjuster->state->name }}</h3>
                <a href="{{ route('adjusters.edit', $log_assignment->assign_adjuster->id) }}" style="float:right; color:white;">Edit</a>
            </div>
            <div class="col-md-12" style="padding: 10px;">
                {{-- <div class="col-md-1"></div> --}}
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('adjusters.show', $log_assignment->assign_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.homestate', $log_assignment->assign_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.corrections', $log_assignment->assign_adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.rejections', $log_assignment->assign_adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
                {{-- <div class="col-md-2"></div> --}}
                <a href="{{ route('tab.adjuster.log-assignments', $log_assignment->assign_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Claim Log Assignments</a>
                <a href="{{ route('tab.adjuster.claim-status', $log_assignment->assign_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

            </div>
            <div class="clearfix"></div>
            <div class="box-body table-responsive no-padding">
                <div class="box-header customHeader">
                    <h3 class="box-title"><b>Claim Number: {{ $log_assignment->claim_number}}</b></h3>
                    <a href="{{ route('log-assignment.edit', $log_assignment->id) }}" style="float:right; color:white;">Edit</a>
                </div>
              <table class="table table-bordered tableStyle">
               <tbody>
                <tr>
                  <th>Field Assign Source</th>
                  <td>{{ $log_assignment->field_assign_source->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>TSI Claim Number</th>
                    <td>{{ $log_assignment->tsi_claim_number ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Cl Assign Type</th>
                    <td>{{ $log_assignment->cl_assign_type->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Assign Rec</th>
                    <td>{{ $log_assignment->date_assign_rec ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Assign Adjuster FullQ</th>
                    <td>{{ $log_assignment->assign_adjuster->getFullName() ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Assigned</th>
                    <td>{{ ($log_assignment->assigned == 1) ? "Yes" : 'No' }}</td>
                </tr>
                <tr>
                    <th>Insured</th>
                    <td>{{ $log_assignment->insured ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Insured City</th>
                    <td>{{ $log_assignment->insured_city ?? "-" }}</td>
                </tr>
                <tr>
                    <th>State</th>
                    <td>{{ $log_assignment->state->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Zip</th>
                    <td>{{ $log_assignment->zip ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Insd Phone</th>
                    <td>{{ $log_assignment->insured_phone_no ?? "-" }}</td>
                </tr>
                 <tr>
                    <th>Carrier</th>
                    <td>{{ $log_assignment->carrier->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Co Claim Number</th>
                    <td>{{ $log_assignment->co_claim_number ?? "-" }}</td>
                </tr>
                <tr>
                    <th>CMS</th>
                    <td>{{ ($log_assignment->cms == 1) ? "Yes" : "No" }}</td>
                </tr>
                <tr>
                    <th>Acknowledgement Sent Attic-Musa, Chubb</th>
                    <td>{{ ($log_assignment->acknowledgement == 1) ? "Yes" : "No" }}</td>
                </tr>
                <tr>
                    <th>Date Contacted</th>
                    <td>{{ $log_assignment->date_contacted ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Inspected</th>
                    <td>{{ $log_assignment->date_inspected ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Report Due</th>
                    <td>{{ $log_assignment->date_report_due ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Rep Returned</th>
                    <td>{{ $log_assignment->date_rep_returned ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date Assign Closed</th>
                    <td>{{ $log_assignment->date_assign_closed ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Close Type</th>
                    <td>{{ $log_assignment->close_type->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date ReOpened</th>
                    <td>{{ $log_assignment->date_reopened ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Date ReClosed</th>
                    <td>{{ $log_assignment->date_reclosed ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Notes</th>
                    <td>{{ $log_assignment->notes ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Maps</th>
                    <td>
                        <a href="{{ $log_assignment->map_link }}" target="_blank">{{ $log_assignment->map_link }}</a>
                    </td>
                </tr>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection

@push('css')
<style type="text/css">
.tableStyle th
{
    width: 20%;
}
</style>
@endpush
