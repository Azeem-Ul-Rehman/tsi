@extends('adminlte::page')
@section('title', 'Adjuster Log Assignment')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">{{ $log_assignment->master_adjuster->getFullName() }} - {{ $log_assignment->master_adjuster->state->name }}</h3>
            <a href="{{ route('adjusters.show', $log_assignment->master_adjuster->id) }}" style="float:right; color:white;">View</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $log_assignment->master_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
            <a href="{{ route('tab.adjuster.homestate', $log_assignment->master_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            {{-- <div class="col-md-2"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.corrections', $log_assignment->master_adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $log_assignment->master_adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.log-assignments', $log_assignment->master_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $log_assignment->master_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

        </div>
        <div class="clearfix"></div>
        <form action="{{ route('log-assignment.update', $log_assignment->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('claim_number') ? 'has-error': '' }}">
                                <label>Claim Number</label>
                                <input type="text" name="claim_number" class="form-control" value="{{ $log_assignment->claim_number }}">
                                @if($errors->has('claim_number'))
                                    <span class="help-block text-danger">{{ $errors->first('claim_number') }}</span>
                                @endif
                            </div>
                        </div>
    
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('tsi_file_number') ? 'has-error': '' }}">
                                <label>TSI File Number</label>
                                <input type="text" name="tsi_file_number" class="form-control" value="{{ $log_assignment->tsi_file_number }}">
                                @if($errors->has('tsi_file_number'))
                                    <span class="help-block text-danger">{{ $errors->first('tsi_file_number') }}</span>
                                @endif
                            </div>
                        </div>
    
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('adjuster') ? 'has-error': '' }}">
                                <label>Adjuster</label>
                                <select name="adjuster" class="form-control">
                                    <option value="">--Select--</option>
                                    @if(!empty($adjusters))
                                        @foreach($adjusters as $adjuster)
                                            <option value="{{ $adjuster->id }}" {{ ($log_assignment->master_adjuster_id == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('adjuster'))
                                    <span class="help-block text-danger">{{ $errors->first('adjuster') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('insured') ? 'has-error': '' }}">
                                <label>Insured</label>
                                <input type="text" name="insured" class="form-control" value="{{ $log_assignment->insured }}">
                                @if($errors->has('insured'))
                                    <span class="help-block text-danger">{{ $errors->first('insured') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('state') ? 'has-error': '' }}">
                                <label>State</label>
                                <select name="state" class="form-control" id="states">
                                    <option value="">--Select--</option>
                                    @if(!empty($states))
                                        @foreach($states as $state)
                                            <option value="{{ $state->id }}" {{ ($log_assignment->state_id == $state->id) ? 'selected' : '' }}>{{ $state->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('state'))
                                    <span class="help-block text-danger">{{ $errors->first('state') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('city') ? 'has-error': '' }}">
                                <label>City</label>
                                <select name="city" class="form-control">
                                    <option value="">--Select--</option>
                                        @foreach($log_assignment->state->cities as $city)
                                            <option value="{{ $city->id }}" {{ ($log_assignment->city_id == $city->id) ? 'selected' : '' }}>{{ $city->name }}</option>
                                        @endforeach
                                </select>
                                @if($errors->has('city'))
                                    <span class="help-block text-danger">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('zip') ? 'has-error': '' }}">
                                <label>Zip</label>
                                <input type="text" name="zip" class="form-control" value="{{ $log_assignment->zip_code }}">
                                @if($errors->has('zip'))
                                    <span class="help-block text-danger">{{ $errors->first('zip') }}</span>
                                @endif
                            </div>
                        </div>
                    
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('date') ? 'has-error': '' }}">
                            <label>Date</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="date" value="{{ $log_assignment->date }}">
                                @if($errors->has('date'))
                                    <span class="help-block text-danger">{{ $errors->first('date') }}</span>
                                @endif
                            </div>
                        </div>
                        </div>
                    </div>
    
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('status') ? 'has-error': '' }}">
                                <label>Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select</option>
                                    <option value="open" {{ ($log_assignment->status == 1) ? 'selected' : '' }}>Open</option>
                                    <option value="close" {{ ($log_assignment->status == 0) ? 'selected' : ''}}>Close</option>
                                </select>
                                @if($errors->has('status'))
                                    <span class="help-block text-danger">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>       
                </div>
            </div> 
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Save</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
         $("#all_states").change( function(){
            var state = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{ route('get.cities') }}",
                data: {state_id: state, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        var options = '';
                        options += '<option value="">--Select--</option>';
                        response.cities.forEach( function(data){
                            options += '<option value="'+ data.id +'">'+ data.name +'</option>';
                        });

                        $("#cities").html(options);
                    }
                }
            })
        });
    </script>
@endpush
