@extends('adminlte::page')
@section('title', 'Log Fields')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Log Field</h3>
            </div>
            <div class="col-md-6 text-right">
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form  method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="claim_number" id="claim_number" value="claim_number" {{$getAllFields->claim_number==1?'checked':''}} onchange="getvalue('claim_number')"> Claim Number
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="master_adjuster_id" value="master_adjuster_id" {{$getAllFields->master_adjuster_id==1?'checked':''}} onchange="getvalue('master_adjuster_id')"> Master Adjuster
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="tsi_file_number" value="tsi_file_number" {{$getAllFields->tsi_file_number==1?'checked':''}} onchange="getvalue('tsi_file_number')"> File Number
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="city_id" value="city_id" {{$getAllFields->city_id==1?'checked':''}} onchange="getvalue('city_id')"> City
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="state_id" value="state_id" {{$getAllFields->state_id==1?'checked':''}} onchange="getvalue('state_id')"> State
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="zip_code" value="zip_code" {{$getAllFields->zip_code==1?'checked':''}} onchange="getvalue('zip_code')"> Zip Code
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="date" value="date" {{$getAllFields->date==1?'checked':''}} onchange="getvalue('date')"> Date
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="company" value="company" {{$getAllFields->company==1?'checked':''}} onchange="getvalue('company')"> Company
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="checkbox" name="fieldcheck[]" id="status" value="status" {{$getAllFields->status==1?'checked':''}} onchange="getvalue('status')"> Status
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function getvalue(Name){
            var status=0;
        if($("#"+Name+"").is(':checked')){
            status=1;
        }
           $.ajax({
                url: "{{ route('logstore') }}",
               type: 'POST',
               data : { status: status,name:Name},
               success: function(response){
                    if(response.status == 'success'){
                     toastr['success']("Log Field been updated.");
                    }
               },
               error: function(error){
                   toastr['error']("Something went wrong.");
               }

            });
        }
    </script>
@endpush
