@extends('adminlte::page')
@section('title', 'Manage Dashboard')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Dashboard Items</h3>
            </div>
        </div>
        <form action="{{ route('dashboard.item.user.store')}}" method="post">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                        @csrf
                        @if(!empty($dashboard_items))
                            @foreach($dashboard_items as $dashboard_item)
                                <div class="form-group">
                                    <input type="checkbox" name="items[]" value="{{ $dashboard_item->id}}"
                                    @if(!empty($already_have_dashboard_items))
                                        @foreach($already_have_dashboard_items as $already_item)
                                            @if($already_item->id == $dashboard_item->id)
                                                checked = 'true'
                                            @endif
                                        @endforeach
                                    @endif> {{ $dashboard_item->name}}
                                </div>
                            @endforeach
                        @endif
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Save</button>
            </div>
            <!-- /.box-body -->
        </form>    
    </div>

@endsection

@push('js')

@endpush
