@extends('adminlte::page')
@section('title', 'View Report')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">{{ $report->master_adjuster->getFullName() }} - {{ $report->claim->claim_number }}
                - {{ ucfirst(str_replace('_',' ',$report->category)) }}</h3>
        </div>
        <div class="clearfix"></div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered tableStyle">
                @if($report->category == 'report')
                    <tbody>

                    <tr>
                        <th>Mortgage</th>
                        <td>{{ $report->mortage ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Policy type and special provisions</th>
                        <td>{{ $report->policy_type_and_special_provisions ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Correct deductible applied</th>
                        <td>{{ $report->correct_deductable_applied ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Time of inspection</th>
                        <td>{{ $report->time_of_inspection ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Date of inspection</th>
                        <td>{{ $report->date_of_inspection ?? '-'}}</td>
                    </tr>
                    <tr>
                        <th>Name of who was present at inspection</th>
                        <td>{{ $report->name_of_who_was_inspect_at_inspection ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Detailed investigation summary</th>
                        <td>{{ $report->detailed_investigation_summary ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Personal property addressed</th>
                        <td>{{ $report->personal_property_addressed ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>FL adjusters noting O&L and if it applies</th>
                        <td>{{ $report->fl_adjusters_nothing_ol_and_if_it_applies ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Detailed COL</th>
                        <td>{{ $report->detailed_col ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Any PA or contractor involved</th>
                        <td>{{ $report->any_pa_or_contracted_involved ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Subrogation addressed</th>
                        <td>{{ $report->subrogation_addressed ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>TSI logo</th>
                        <td>{{ $report->tsi_logo ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Miscellaneous</th>
                        <td>{{ $report->miscellaneous ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Report Format</th>
                        <td>{{ $report->report_format ?? "-" }}</td>
                    </tr>

                    </tbody>
                @endif
                @if($report->category == 'estimate')
                    <tbody>

                    <tr>
                        <th>Price list current</th>
                        <td>{{ $report->price_list_current ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Minimum charges applied</th>
                        <td>{{ $report->minimum_charges_applied ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Adding O&P when necessary</th>
                        <td>{{ $report->adding_op_when_necessary ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Apply correct depreciation</th>
                        <td>{{ $report->applied_correct_deprication ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Bid items applied correctly</th>
                        <td>{{ $report->bid_items_applied_correctly ?? '-'}}</td>
                    </tr>
                    <tr>
                        <th>Deductible</th>
                        <td>{{ $report->deductible ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Items under correct coverage</th>
                        <td>{{ $report->items_under_correct_coverage ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Cabinets</th>
                        <td>{{ $report->cabinets ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Drywall – accurate</th>
                        <td>{{ $report->dry_wall_accurate ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Debris removal</th>
                        <td>{{ $report->debris_removal ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Fencing</th>
                        <td>{{ $report->fencing ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Flooring</th>
                        <td>{{ $report->flooring ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Mask and prep</th>
                        <td>{{ $report->mask_and_prep ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Insulation</th>
                        <td>{{ $report->insulation ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Content manipulation</th>
                        <td>{{ $report->content_manipulation ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Painting</th>
                        <td>{{ $report->painting ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Roof</th>
                        <td>{{ $report->roof ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Siding</th>
                        <td>{{ $report->siding ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Sketch</th>
                        <td>{{ $report->sketch ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Tile</th>
                        <td>{{ $report->tiles ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Trees</th>
                        <td>{{ $report->trees ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Contents</th>
                        <td>{{ $report->contents ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Wire Shelving proper code</th>
                        <td>{{ $report->wire_shelving_proper_code ?? "-" }}</td>
                    </tr>


                    </tbody>
                @endif
                @if($report->category == 'photos')
                    <tbody>

                    <tr>
                        <th>Photos documenting COL</th>
                        <td>{{ $report->photos_documenting_col ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Photos of damaged/undamaged rooms</th>
                        <td>{{ $report->photos_of_damaged_or_undamaged_col ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Details in description</th>
                        <td>{{ $report->details_in_description ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Elevation photos</th>
                        <td>{{ $report->elevation_photos ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Address verification</th>
                        <td>{{ $report->address_verification ?? '-'}}</td>
                    </tr>
                    <tr>
                        <th>All slopes of roofs</th>
                        <td>{{ $report->all_slopes_of_roofs ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Fence attached (hurricane only)</th>
                        <td>{{ $report->fence_attached ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Attic</th>
                        <td>{{ $report->attic ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Valley metal</th>
                        <td>{{ $report->valley_metal ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Drip edge</th>
                        <td>{{ $report->drip_edge ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Shingle gauge</th>
                        <td>{{ $report->shingle_gauge ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Pitch gauge</th>
                        <td>{{ $report->pitch_gauge ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Labeled correctly</th>
                        <td>{{ $report->labeled_correctly ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Name taken by</th>
                        <td>{{ $report->name_taken_by ?? "-" }}</td>
                    </tr>
                    </tbody>
                @endif
                @if($report->category == 'additional_documents')
                    <tbody>

                    <tr>
                        <th>Survey</th>
                        <td>{{ $report->survey ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>Roof form</th>
                        <td>{{ $report->roof_form ?? "-" }}</td>
                    </tr>
                    <tr>
                        <th>MISC.</th>
                        <td>{{ $report->misc ?? "-" }}</td>
                    </tr>
                    </tbody>
                @endif

            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')
    <style type="text/css">
        .tableStyle th {
            width: 20%;
        }
    </style>
@endpush
