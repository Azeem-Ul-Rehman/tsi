@extends('adminlte::page')
@section('title', 'Add Report')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">Add Report Form</h3>
        </div>
        <form action="{{ route('report.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('adjuster_id') ? 'has-error': '' }}">
                                <label>Adjuster</label>
                                <select name="adjuster_id" class="form-control adjuster" id="adjuster">
                                    <option value="">--Select--</option>
                                    @if(!empty($adjusters))
                                        @foreach($adjusters as $adjuster)
                                            <option
                                                value="{{ $adjuster->id }}" {{ (old('adjuster_id') == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('adjuster_id'))
                                    <span class="help-block text-danger">{{ $errors->first('adjuster_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('claim_id') ? 'has-error': '' }}">
                                <label>Claim</label>
                                <select name="claim_id" class="form-control claim_id" id="claim_id">
                                    <option value="">--Select--</option>
                                </select>
                                @if($errors->has('claim_id'))
                                    <span class="help-block text-danger">{{ $errors->first('claim_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('category') ? 'has-error': '' }}">
                                <label>Category</label>
                                <select name="category" class="form-control category" id="category">
                                    <option value="">--Select--</option>
                                    <option value="report">Report</option>
                                    <option value="estimate">Estimate</option>
                                    <option value="photos">Photos</option>
                                    <option value="additional_documents">Additional Documents</option>
                                </select>
                                @if($errors->has('category'))
                                    <span class="help-block text-danger">{{ $errors->first('category') }}</span>
                                @endif
                            </div>
                        </div>


                        {{--Report Category--}}
                        <div id="reportCategory" style="display: none">
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('mortage') ? 'has-error': '' }}">
                                    <label>Mortgage</label>
                                    <input type="text" name="mortage" class="form-control"
                                           value="{{ old('mortage') }}">
                                    @if($errors->has('mortage'))
                                        <span class="help-block text-danger">{{ $errors->first('mortage') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('policy_type_and_special_provisions') ? 'has-error': '' }}">
                                    <label>Policy type and special provisions</label>

                                    <input type="text" class="form-control "
                                           name="policy_type_and_special_provisions"
                                           value="{{ old('policy_type_and_special_provisions') }}">
                                    @if($errors->has('policy_type_and_special_provisions'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('policy_type_and_special_provisions') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('correct_deductable_applied') ? 'has-error': '' }}">
                                    <label>Correct deductible applied</label>
                                    <input type="text" name="correct_deductable_applied" class="form-control"
                                           value="{{ old('correct_deductable_applied') }}">
                                    @if($errors->has('correct_deductable_applied'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('correct_deductable_applied') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('time_of_inspection') ? 'has-error': '' }}">
                                    <label>Time of inspection</label>
                                    <input type="text" name="time_of_inspection" class="form-control"
                                           value="{{ old('time_of_inspection') }}">
                                    @if($errors->has('time_of_inspection'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('time_of_inspection') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('date_of_inspection') ? 'has-error': '' }}">
                                    <label>Date of inspection</label>
                                    <input type="text" name="date_of_inspection" class="form-control"
                                           value="{{ old('date_of_inspection') }}">
                                    @if($errors->has('date_of_inspection'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('date_of_inspection') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('name_of_who_was_inspect_at_inspection') ? 'has-error': '' }}">
                                    <label>Name of who was present at inspection</label>

                                    <input type="text" class="form-control "
                                           name="name_of_who_was_inspect_at_inspection"
                                           value="{{ old('name_of_who_was_inspect_at_inspection') }}">
                                    @if($errors->has('name_of_who_was_inspect_at_inspection'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('name_of_who_was_inspect_at_inspection') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('detailed_investigation_summary') ? 'has-error': '' }}">
                                    <label>Detailed investigation summary</label>

                                    <input type="text" class="form-control "
                                           name="detailed_investigation_summary"
                                           value="{{ old('detailed_investigation_summary') }}">
                                    @if($errors->has('detailed_investigation_summary'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('detailed_investigation_summary') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('personal_property_addressed') ? 'has-error': '' }}">
                                    <label>Personal property addressed
                                    </label>

                                    <input type="text" class="form-control "
                                           name="personal_property_addressed"
                                           value="{{ old('personal_property_addressed') }}">
                                    @if($errors->has('personal_property_addressed'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('personal_property_addressed') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('fl_adjusters_nothing_ol_and_if_it_applies') ? 'has-error': '' }}">
                                    <label>FL adjusters noting O&L and if it applies</label>
                                    <input type="text" name="fl_adjusters_nothing_ol_and_if_it_applies"
                                           class="form-control"
                                           value="{{ old('fl_adjusters_nothing_ol_and_if_it_applies') }}">
                                    @if($errors->has('fl_adjusters_nothing_ol_and_if_it_applies'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('fl_adjusters_nothing_ol_and_if_it_applies') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('detailed_col') ? 'has-error': '' }}">
                                    <label>Detailed COL</label>
                                    <input type="text" name="detailed_col" class="form-control"
                                           value="{{ old('detailed_col') }}">
                                    @if($errors->has('detailed_col'))
                                        <span class="help-block text-danger">{{ $errors->first('detailed_col') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('any_pa_or_contracted_involved') ? 'has-error': '' }}">
                                    <label>Any PA or contractor involved</label>
                                    <input type="text" name="any_pa_or_contracted_involved" class="form-control"
                                           value="{{ old('any_pa_or_contracted_involved') }}">
                                    @if($errors->has('any_pa_or_contracted_involved'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('any_pa_or_contracted_involved') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('subrogation_addressed') ? 'has-error': '' }}">
                                    <label>Subrogation addressed</label>
                                    <input type="text" name="subrogation_addressed" class="form-control"
                                           value="{{ old('subrogation_addressed') }}">
                                    @if($errors->has('subrogation_addressed'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('subrogation_addressed') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('tsi_logo') ? 'has-error': '' }}">
                                    <label>TSI Logo</label>
                                    <input type="text" name="tsi_logo" class="form-control"
                                           value="{{ old('tsi_logo') }}">
                                    @if($errors->has('tsi_logo'))
                                        <span class="help-block text-danger">{{ $errors->first('tsi_logo') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('miscellaneous') ? 'has-error': '' }}">
                                    <label>Miscellaneous</label>
                                    <input type="text" name="miscellaneous" class="form-control"
                                           value="{{ old('miscellaneous') }}">
                                    @if($errors->has('miscellaneous'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('miscellaneous') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('report_format') ? 'has-error': '' }}">
                                    <label>Report Format</label>
                                    <input type="text" name="report_format" class="form-control"
                                           value="{{ old('report_format') }}">
                                    @if($errors->has('report_format'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('report_format') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{--Estiamte Category--}}
                        <div id="estimateCategory" style="display: none">
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('price_list_current') ? 'has-error': '' }}">
                                    <label>Price list current</label>
                                    <input type="text" name="price_list_current" class="form-control"
                                           value="{{ old('price_list_current') }}">
                                    @if($errors->has('price_list_current'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('price_list_current') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('minimum_charges_applied') ? 'has-error': '' }}">
                                    <label>Minimum charges applied </label>
                                    <input type="text" name="minimum_charges_applied" class="form-control"
                                           value="{{ old('minimum_charges_applied') }}">
                                    @if($errors->has('minimum_charges_applied'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('minimum_charges_applied') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('adding_op_when_necessary') ? 'has-error': '' }}">
                                    <label>Adding O&P when necessary</label>
                                    <input type="text" name="adding_op_when_necessary" class="form-control"
                                           value="{{ old('adding_op_when_necessary') }}">
                                    @if($errors->has('adding_op_when_necessary'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('adding_op_when_necessary') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('applied_correct_deprication') ? 'has-error': '' }}">
                                    <label>Apply correct depreciation </label>
                                    <input type="text" name="applied_correct_deprication" class="form-control"
                                           value="{{ old('applied_correct_deprication') }}">
                                    @if($errors->has('applied_correct_deprication'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('applied_correct_deprication') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('bid_items_applied_correctly') ? 'has-error': '' }}">
                                    <label>Bid items applied correctly</label>
                                    <input type="text" name="bid_items_applied_correctly" class="form-control"
                                           value="{{ old('bid_items_applied_correctly') }}">
                                    @if($errors->has('bid_items_applied_correctly'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('bid_items_applied_correctly') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('deductible') ? 'has-error': '' }}">
                                    <label>Deductible </label>
                                    <input type="text" name="deductible" class="form-control"
                                           value="{{ old('deductible') }}">
                                    @if($errors->has('deductible'))
                                        <span class="help-block text-danger">{{ $errors->first('deductible') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('items_under_correct_coverage') ? 'has-error': '' }}">
                                    <label>Items under correct coverage </label>
                                    <input type="text" name="items_under_correct_coverage" class="form-control"
                                           value="{{ old('items_under_correct_coverage') }}">
                                    @if($errors->has('items_under_correct_coverage'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('items_under_correct_coverage') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('cabinets') ? 'has-error': '' }}">
                                    <label>Cabinets </label>
                                    <input type="text" name="cabinets" class="form-control"
                                           value="{{ old('cabinets') }}">
                                    @if($errors->has('cabinets'))
                                        <span class="help-block text-danger">{{ $errors->first('cabinets') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('dry_wall_accurate') ? 'has-error': '' }}">
                                    <label>Drywall – accurate </label>
                                    <input type="text" name="dry_wall_accurate" class="form-control"
                                           value="{{ old('dry_wall_accurate') }}">
                                    @if($errors->has('dry_wall_accurate'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('dry_wall_accurate') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('debris_removal') ? 'has-error': '' }}">
                                    <label>Debris removal </label>
                                    <input type="text" name="debris_removal" class="form-control"
                                           value="{{ old('debris_removal') }}">
                                    @if($errors->has('debris_removal'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('debris_removal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('fencing') ? 'has-error': '' }}">
                                    <label>Fencing</label>
                                    <input type="text" name="fencing" class="form-control"
                                           value="{{ old('fencing') }}">
                                    @if($errors->has('fencing'))
                                        <span class="help-block text-danger">{{ $errors->first('fencing') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('flooring') ? 'has-error': '' }}">
                                    <label>Flooring</label>
                                    <input type="text" name="flooring" class="form-control"
                                           value="{{ old('flooring') }}">
                                    @if($errors->has('flooring'))
                                        <span class="help-block text-danger">{{ $errors->first('flooring') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('mask_and_prep') ? 'has-error': '' }}">
                                    <label>Mask and prep</label>
                                    <input type="text" name="mask_and_prep" class="form-control"
                                           value="{{ old('mask_and_prep') }}">
                                    @if($errors->has('mask_and_prep'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('mask_and_prep') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('insulation') ? 'has-error': '' }}">
                                    <label>Insulation</label>
                                    <input type="text" name="insulation" class="form-control"
                                           value="{{ old('insulation') }}">
                                    @if($errors->has('insulation'))
                                        <span class="help-block text-danger">{{ $errors->first('insulation') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('content_manipulation') ? 'has-error': '' }}">
                                    <label>Content manipulation </label>
                                    <input type="text" name="content_manipulation" class="form-control"
                                           value="{{ old('content_manipulation') }}">
                                    @if($errors->has('content_manipulation'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('content_manipulation') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('painting') ? 'has-error': '' }}">
                                    <label>Painting</label>
                                    <input type="text" name="painting" class="form-control"
                                           value="{{ old('painting') }}">
                                    @if($errors->has('painting'))
                                        <span class="help-block text-danger">{{ $errors->first('painting') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('roof') ? 'has-error': '' }}">
                                    <label>Roof</label>
                                    <input type="text" name="roof" class="form-control"
                                           value="{{ old('roof') }}">
                                    @if($errors->has('roof'))
                                        <span class="help-block text-danger">{{ $errors->first('roof') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('siding') ? 'has-error': '' }}">
                                    <label>Siding</label>
                                    <input type="text" name="siding" class="form-control"
                                           value="{{ old('siding') }}">
                                    @if($errors->has('siding'))
                                        <span class="help-block text-danger">{{ $errors->first('siding') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('sketch') ? 'has-error': '' }}">
                                    <label>Sketch</label>
                                    <input type="text" name="sketch" class="form-control"
                                           value="{{ old('sketch') }}">
                                    @if($errors->has('sketch'))
                                        <span class="help-block text-danger">{{ $errors->first('sketch') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('tiles') ? 'has-error': '' }}">
                                    <label>Tile</label>
                                    <input type="text" name="tiles" class="form-control"
                                           value="{{ old('tiles') }}">
                                    @if($errors->has('tiles'))
                                        <span class="help-block text-danger">{{ $errors->first('tiles') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('trees') ? 'has-error': '' }}">
                                    <label>Trees</label>
                                    <input type="text" name="trees" class="form-control"
                                           value="{{ old('trees') }}">
                                    @if($errors->has('trees'))
                                        <span class="help-block text-danger">{{ $errors->first('trees') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('contents') ? 'has-error': '' }}">
                                    <label>Contents</label>
                                    <input type="text" name="contents" class="form-control"
                                           value="{{ old('contents') }}">
                                    @if($errors->has('contents'))
                                        <span class="help-block text-danger">{{ $errors->first('contents') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('wire_shelving_proper_code') ? 'has-error': '' }}">
                                    <label>Wire Shelving proper code </label>
                                    <input type="text" name="wire_shelving_proper_code" class="form-control"
                                           value="{{ old('wire_shelving_proper_code') }}">
                                    @if($errors->has('wire_shelving_proper_code'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('wire_shelving_proper_code') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{--Photos Category--}}
                        <div id="photosCategory" style="display: none">
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('photos_documenting_col') ? 'has-error': '' }}">
                                    <label>Photos documenting COL</label>
                                    <input type="text" name="photos_documenting_col" class="form-control"
                                           value="{{ old('photos_documenting_col') }}">
                                    @if($errors->has('photos_documenting_col'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('photos_documenting_col') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div
                                    class="form-group {{ $errors->has('photos_of_damaged_or_undamaged_col') ? 'has-error': '' }}">
                                    <label>Photos of damaged/undamaged rooms</label>
                                    <input type="text" name="photos_of_damaged_or_undamaged_col" class="form-control"
                                           value="{{ old('photos_of_damaged_or_undamaged_col') }}">
                                    @if($errors->has('photos_of_damaged_or_undamaged_col'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('photos_of_damaged_or_undamaged_col') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('details_in_description') ? 'has-error': '' }}">
                                    <label>Details in description</label>
                                    <input type="text" name="details_in_description" class="form-control"
                                           value="{{ old('details_in_description') }}">
                                    @if($errors->has('details_in_description'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('details_in_description') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('elevation_photos') ? 'has-error': '' }}">
                                    <label>Elevation photos</label>
                                    <input type="text" name="elevation_photos" class="form-control"
                                           value="{{ old('elevation_photos') }}">
                                    @if($errors->has('elevation_photos'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('elevation_photos') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('address_verification') ? 'has-error': '' }}">
                                    <label>Address verification</label>
                                    <input type="text" name="address_verification" class="form-control"
                                           value="{{ old('address_verification') }}">
                                    @if($errors->has('address_verification'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('address_verification') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('all_slopes_of_roofs') ? 'has-error': '' }}">
                                    <label>All slopes of roofs</label>
                                    <input type="text" name="all_slopes_of_roofs" class="form-control"
                                           value="{{ old('all_slopes_of_roofs') }}">
                                    @if($errors->has('all_slopes_of_roofs'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('all_slopes_of_roofs') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('fence_attached') ? 'has-error': '' }}">
                                    <label>Fence attached (hurricane only)</label>
                                    <input type="text" name="fence_attached" class="form-control"
                                           value="{{ old('fence_attached') }}">
                                    @if($errors->has('fence_attached'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('fence_attached') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('attic') ? 'has-error': '' }}">
                                    <label>Attic</label>
                                    <input type="text" name="attic" class="form-control"
                                           value="{{ old('attic') }}">
                                    @if($errors->has('attic'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('attic') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('valley_metal') ? 'has-error': '' }}">
                                    <label>Valley metal</label>
                                    <input type="text" name="valley_metal" class="form-control"
                                           value="{{ old('valley_metal') }}">
                                    @if($errors->has('valley_metal'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('valley_metal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('drip_edge') ? 'has-error': '' }}">
                                    <label>Drip edge</label>
                                    <input type="text" name="drip_edge" class="form-control"
                                           value="{{ old('drip_edge') }}">
                                    @if($errors->has('drip_edge'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('drip_edge') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('shingle_gauge') ? 'has-error': '' }}">
                                    <label>Shingle gauge</label>
                                    <input type="text" name="shingle_gauge" class="form-control"
                                           value="{{ old('shingle_gauge') }}">
                                    @if($errors->has('shingle_gauge'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('shingle_gauge') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('pitch_gauge') ? 'has-error': '' }}">
                                    <label>Pitch gauge</label>
                                    <input type="text" name="pitch_gauge" class="form-control"
                                           value="{{ old('pitch_gauge') }}">
                                    @if($errors->has('pitch_gauge'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('pitch_gauge') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('labeled_correctly') ? 'has-error': '' }}">
                                    <label>Labeled correctly</label>
                                    <input type="text" name="labeled_correctly" class="form-control"
                                           value="{{ old('labeled_correctly') }}">
                                    @if($errors->has('labeled_correctly'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('labeled_correctly') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('name_taken_by') ? 'has-error': '' }}">
                                    <label>Name taken by</label>
                                    <input type="text" name="name_taken_by" class="form-control"
                                           value="{{ old('name_taken_by') }}">
                                    @if($errors->has('name_taken_by'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('name_taken_by') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        {{--Additional Documents--}}
                        <div id="documentsCategory" style="display: none">
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('survey') ? 'has-error': '' }}">
                                    <label>Survey
                                    </label>
                                    <input type="text" name="survey" class="form-control"
                                           value="{{ old('survey') }}">
                                    @if($errors->has('survey'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('survey') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('roof_form') ? 'has-error': '' }}">
                                    <label>Roof form</label>
                                    <input type="text" name="roof_form" class="form-control"
                                           value="{{ old('roof_form') }}">
                                    @if($errors->has('roof_form'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('roof_form') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {{ $errors->has('misc') ? 'has-error': '' }}">
                                    <label>MISC.</label>
                                    <input type="text" name="misc" class="form-control"
                                           value="{{ old('misc') }}">
                                    @if($errors->has('misc'))
                                        <span
                                            class="help-block text-danger">{{ $errors->first('misc') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        {{--                        <div class="col-md-3">--}}
                        {{--                            <div class="form-group">--}}
                        {{--                                <label for="note">Note</label><br>--}}
                        {{--                                <textarea name="note" id="note" class="form-control">{{ old('note') }}</textarea>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Save</button>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        $("#adjuster").change(function () {
            var adjuster = $(this).val();
            $.ajax({
                method: "POST",
                url: "{{ route('get.claims') }}",
                data: {adjuster_id: adjuster, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        var options = '';
                        options += '<option value="">--Select--</option>';
                        response.claims.forEach(function (data) {
                            options += '<option value="' + data.id + '">' + data.claim_number + '</option>';
                        });

                        $("#claim_id").html(options);
                    }
                }
            })
        });

        $("#category").change(function () {

            $('#reportCategory').hide();
            $('#estimateCategory').hide();
            $('#photosCategory').hide();
            $('#documentsCategory').hide();

            if ($(this).val() == 'report') {
                $('#reportCategory').show();
            }
            if ($(this).val() == 'estimate') {
                $('#estimateCategory').show();
            }
            if ($(this).val() == 'photos') {
                $('#photosCategory').show();
            }
            if ($(this).val() == 'additional_documents') {
                $('#documentsCategory').show();
            }
        });

    </script>
@endpush
