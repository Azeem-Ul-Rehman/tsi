@extends('adminlte::page')
@section('title', 'All Reports')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Reports</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('report.create') }}" class="btn btn-primary btnTheme">Create Report</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Adjuster</th>
                    <th>Claim Number</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($reports))
                    @foreach($reports as $report)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $report->master_adjuster->getFullName() ?? '--' }}</td>
                            <td>{{ $report->claim->claim_number ?? '--' }}</td>
                            <td>{{ ucwords(str_replace('_',' ',$report->category)) ?? '--' }}</td>
                            <td>
                                <a href="{{ route('report.edit', $report->id) }}">
                                    <button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i>
                                    </button>
                                </a>
                                <a href="{{ route('report.show', $report->id) }}">
                                    <button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button>
                                </a>
                                <form action="{{ route('report.destroy', $report->id) }}" method="POST"
                                      class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip"
                                            title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
