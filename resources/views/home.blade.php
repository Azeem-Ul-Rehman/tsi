@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>

@stop

@section('content')
    <div class="row">
        @if(!empty($items))
            @foreach($items as $item)
                @foreach ($item as $key => $value)
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{ $value }}</h3>
                                <p>{{ $key }}</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="#" class="small-box-footer" onclick="getadjusterslug('{{$key}}')">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endforeach
            @endforeach
        @endif
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$adj_corrections}}</h3>
                    <p>Total Corrections</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{ url('adjuster-corrections') }}" class="small-box-footer">More info <i
                        class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        function getadjusterslug(adjusterslug) {
            $.ajax({
                method: "POST",
                url: "{{ route('adjustermasterslug') }}",
                data: {adjusterslug: adjusterslug, _token: '{{csrf_token()}}'},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        if (adjusterslug != 'Total States') {
                            window.location.href = "{{route('adjusters.index')}}";
                        } else {
                            window.location.href = "{{route('states.index')}}";
                        }
                    }
                }
            });
        }
    </script>
@endpush

