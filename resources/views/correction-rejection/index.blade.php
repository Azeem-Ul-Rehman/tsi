@extends('adminlte::page')
@section('title', 'All Correction Rejections')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Correction Rejections</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('correction-rejections.create') }}" class="btn btn-primary btnTheme">Create Correction Rejection</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Review Date</th>
                    <th>Field Adjuster</th>
                    <th>Claim Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($correction_rejections))
                    @foreach($correction_rejections as $correction_rejection)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $correction_rejection->review_date ?? '--' }}</td>
                            <td>{{ $correction_rejection->field_adjuster?$correction_rejection->field_adjuster->getFullName():'--' }}</td>
                            <td>{{ $correction_rejection->claim_number ?? '--' }}</td>
                            <td>
                                <a href="{{ route('correction-rejections.edit', $correction_rejection->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="{{ route('correction-rejections.show', $correction_rejection->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="{{ route('correction-rejections.destroy', $correction_rejection->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
