@extends('adminlte::page')
@section('title', 'Edit State')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit State</h3>
        </div>
        <form action="{{ route('states.update', $state->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $state->name }}" required>
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('short_name') ? 'has-error' : '' }}">
                            <label>Short name/Abbrevation</label>
                            <input type="text" name="short_name" class="form-control" value="{{ $state->short_name }}" required>
                            @if($errors->has('short_name'))
                                <span class="help-block text-danger">{{ $errors->first('short_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection