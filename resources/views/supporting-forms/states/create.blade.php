@extends('adminlte::page')
@section('title', 'Add State')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New State</h3>
        </div>
        <form action="{{ route('states.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('short_name') ? 'has-error' : '' }}">
                            <label>Short name/Abbrevation</label>
                            <input type="text" name="short_name" class="form-control" value="{{ old('short_name') }}" required>
                            @if($errors->has('short_name'))
                                <span class="help-block text-danger">{{ $errors->first('short_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection