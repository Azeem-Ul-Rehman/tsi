@extends('adminlte::page')
@section('title', 'Supporting Forms')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">Supporting Forms</h3>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="20%">Form</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Adjuster Grade</td>
                        <td>
                            <a href="{{ route('adjuster-grades.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('adjuster-grades.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Grade Reasons</td>
                        <td>
                            <a href="{{ route('grade-reasons.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('grade-reasons.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Inactive Reasons</td>
                        <td>
                            <a href="{{ route('inactive-reasons.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('inactive-reasons.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Cities</td>
                        <td>
                            <a href="{{ route('cities.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('cities.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>States</td>
                        <td>
                            <a href="{{ route('states.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('states.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>State Service Area</td>
                        <td>
                            <a href="{{ route('service-state-area.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('service-state-area.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Roles</td>
                        <td>
                            <a href="{{ route('roles.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('roles.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Fluent Languages</td>
                        <td>
                            <a href="{{ route('languages.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('languages.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Specialities</td>
                        <td>
                            <a href="{{ route('specialties.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('specialties.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Carriers</td>
                        <td>
                            <a href="{{ route('carriers.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('carriers.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Field Assign Sources</td>
                        <td>
                            <a href="{{ route('field-assign-sources.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('field-assign-sources.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>

                    <tr>
                        <td>Cl Assign Type</td>
                        <td>
                            <a href="{{ route('cl-assign-types.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('cl-assign-types.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>
                    <tr>
                        <td>Personnel Status</td>
                        <td>
                            <a href="{{ route('personal-status.create') }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                            <a href="{{ route('personal-status.index') }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
