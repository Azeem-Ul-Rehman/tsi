@extends('adminlte::page')
@section('title', 'All Fluent Languages')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Fluent Languages</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('languages.create') }}" class="btn btn-primary btnTheme">Create Fluent Language</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Language</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($languages))
                    @foreach($languages as $language)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $language->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('languages.edit', $language->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <form action="{{ route('languages.destroy', $language->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
