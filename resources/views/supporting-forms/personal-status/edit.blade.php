@extends('adminlte::page')
@section('title', 'Edit Personnel Status')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Personnel Status</h3>
        </div>
        <form action="{{ route('personal-status.update', $personal_status->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Reason</label>
                            <input type="text" name="name" value="{{ $personal_status->name }}" class="form-control">
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection
