@extends('adminlte::page')
@section('title', 'AssignRole')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Role</h3>
        </div>
        <form action="{{ route('roles.update', $role->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
                            <label>Role</label>
                            <input type="text" name="role" class="form-control" value="{{ $role->name }}" required>
                            @if($errors->has('role'))
                                <span class="help-block text-danger">{{ $errors->first('role') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection