@extends('adminlte::page')
@section('title', 'All Adjuster Grade')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Adjuster Grades</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('adjuster-grades.create') }}" class="btn btn-primary btnTheme">Create Adjuster Grade</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Grade</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($adjuster_grades))
                    @foreach($adjuster_grades as $adjuster_grade)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $adjuster_grade->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('adjuster-grades.edit', $adjuster_grade->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                {{--<a href=""><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>--}}
                                <form action="{{ route('adjuster-grades.destroy', $adjuster_grade->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
