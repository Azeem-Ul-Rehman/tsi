@extends('adminlte::page')
@section('title', 'All Cl Assign Types')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Cl Assign Types</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('cl-assign-types.create') }}" class="btn btn-primary btnTheme">Create Cl Assign Type</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($cl_assign_types))
                    @foreach($cl_assign_types as $cl_assign_type)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $cl_assign_type->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('cl-assign-types.edit', $cl_assign_type->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <form action="{{ route('cl-assign-types.destroy', $cl_assign_type->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
