<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\AssignRole::insert([
           [
               'name' => 'super-admin'
           ],
           [
               'name' => 'user'
           ]
        ]);
    }
}
