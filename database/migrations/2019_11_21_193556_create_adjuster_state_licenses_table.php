<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjusterStateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuster_state_licenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('master_adjuster_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->string('license_number')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('npn_number')->nullable();
            $table->boolean('home_state')->default(0);
            $table->boolean('active')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjuster_state_licenses');
    }
}
