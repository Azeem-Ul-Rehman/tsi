<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrectionRejectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correction_rejections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('adjuster_correction_id');
            $table->string('field_glr_number_exc')->nullable();
            $table->string('field_est_number_exc')->nullable();
            $table->string('field_photo_number_exc')->nullable();
            $table->string('field_total_numner_exc')->nullable();
            $table->string('qa_glr_number_exc')->nullable();
            $table->string('qa_est_number_exc')->nullable();
            $table->string('qa_photo_number_exc')->nullable();
            $table->string('qa_total_number_exc')->nullable();

            $table->boolean('glr_cov_narrative')->nullable();
            $table->boolean('date_ctc_insp')->nullable();
            $table->boolean('mortgage_co')->nullable();
            $table->boolean('col_cause_of_loss')->nullable();
            $table->boolean('subro_salv_details')->nullable();
            $table->boolean('wind_hail_rpt')->nullable();
            $table->boolean('new_survey')->nullable();
            $table->boolean('logo_exc')->nullable();
            $table->boolean('other_glr_exc')->nullable();
            $table->boolean('current_xact')->nullable();
            $table->boolean('deductible_application')->nullable();
            $table->boolean('no_cov_dam_dele_part_all')->nullable();
            $table->boolean('overhead_and_profit')->nullable();
            $table->boolean('measure_accuracy')->nullable();
            $table->boolean('sketch')->nullable();
            $table->boolean('repair_vs_replace')->nullable();
            $table->boolean('acv_vs_rcv')->nullable();
            $table->boolean('depreciation')->nullable();
            $table->boolean('line_item_exc')->nullable();
            $table->boolean('contents_exc')->nullable();
            $table->boolean('drywall')->nullable();
            $table->boolean('insulation')->nullable();
            $table->boolean('cleaning_mit')->nullable();
            $table->boolean('paint')->nullable();
            $table->boolean('trim_and_base')->nullable();
            $table->boolean('flooring')->nullable();
            $table->boolean('cabinets')->nullable();
            $table->boolean('debris_removal')->nullable();
            $table->boolean('other_est_exc')->nullable();
            $table->boolean('oth_structure_proper_items')->nullable();
            $table->boolean('oth_str_acv_vs_rcv')->nullable();
            $table->boolean('oth_str_other_exc')->nullable();
            $table->boolean('roof_wind_bp')->nullable();
            $table->boolean('roof_hail_bp')->nullable();
            $table->boolean('roof_valley_or_ridge')->nullable();
            $table->boolean('roof_repair_vs_replace')->nullable();
            $table->boolean('roof_meas_or_quantity')->nullable();
            $table->boolean('roof_miss_items')->nullable();
            $table->boolean('roof_add_labor')->nullable();
            $table->boolean('roof_other_exc')->nullable();
            $table->boolean('photo_bp')->nullable();
            $table->boolean('photo_clear')->nullable();
            $table->boolean('photo_all_damage')->nullable();
            $table->boolean('photo_label')->nullable();
            $table->boolean('exception_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correction_rejections');
    }
}
