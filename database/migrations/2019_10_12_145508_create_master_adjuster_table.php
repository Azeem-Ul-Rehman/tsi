<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterAdjusterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_adjusters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('initial_service_date')->nullable();
            $table->integer('adjuster_grade_id')->unsigned();
            $table->integer('grade_reason_id')->unsigned();
            $table->date('dob')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('primary_phone_no')->nullable();
            $table->string('secondary_phone_no')->nullable();
            $table->string('emergency_contact_name')->nullable();
            $table->string('emergency_phone_no')->nullable();
            $table->string('email')->nullable();
            $table->text('street_number')->nullable();
            $table->integer('city_id')->unsigned();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->integer('assign_type_id')->unsigned();
            $table->string('driver_license')->nullable();
            $table->string('dl_state')->nullable();
            $table->string('company_llc_name')->nullable();
            $table->string('xact_address')->nullable();
            $table->integer('state_service_area_id')->unsigned();
            $table->integer('state_service_area_t1_id')->unsigned();
            $table->boolean('active')->default(0);
            $table->text('current_active_state')->nullable();
            $table->integer('personal_inactive_reason_id')->unsigned();
            $table->string('tx_id')->nullable();
            $table->string('license_name')->nullable();
            $table->string('company_tin')->nullable();
            $table->string('state_license')->nullable();
            $table->text('speciality')->nullable();
            $table->string('personal_folder_made')->default(0);
            $table->text('fluent_languages')->nullable();
            $table->string('npn_number')->nullable();
            $table->date('check_background_date')->nullable();
            $table->text('comment')->nullable();
            $table->text('url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master-adjuster');
    }
}
