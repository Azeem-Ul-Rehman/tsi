<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInactiveStartDateToMasterAdjustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            $table->date('inactive_start_date')->nullable()->after('comment');
            $table->date('inactive_end_date')->nullable()->after('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            //
        });
    }
}
