<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjusterCorrectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuster_corrections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('master_adjuster_id');
            $table->date('review_date')->nullable();
            $table->boolean('catastrophe_claim')->default(0);
            $table->unsignedInteger('carrier_id');
            $table->string('claim_number')->nullable();
            $table->string('insd_last_name')->nullable();
            $table->unsignedInteger('qa_id');
            $table->unsignedInteger('leader_id');
            $table->integer('fa_glr_correction')->nullable();
            $table->integer('fa_est_correction')->nullable();
            $table->integer('fa_photo_correction')->nullable();
            $table->integer('fa_total_correction')->nullable();

            $table->integer('qa_glr_correction')->nullable();
            $table->integer('qa_est_correction')->nullable();
            $table->integer('qa_photo_correction')->nullable();
            $table->integer('qa_total_correction')->nullable();

            $table->text('correction_detail')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjuster_correction');
    }
}
