<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGradeCommentsColumnToMasterAdjustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            $table->longText('grade_comments')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            $table->dropColumn('grade_comments');
        });
    }
}
