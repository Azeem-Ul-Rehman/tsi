<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('claim_id')->nullable();
            $table->unsignedBigInteger('adjuster_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();
            $table->longText('category')->nullable();

            //  Category one report
            $table->longText('mortage')->nullable();
            $table->longText('policy_type_and_special_provisions')->nullable();
            $table->longText('correct_deductable_applied')->nullable();
            $table->longText('time_of_inspection')->nullable();
            $table->longText('date_of_inspection')->nullable();
            $table->longText('name_of_who_was_inspect_at_inspection')->nullable();
            $table->longText('detailed_investigation_summary')->nullable();
            $table->longText('personal_property_addressed')->nullable();
            $table->longText('fl_adjusters_nothing_ol_and_if_it_applies')->nullable();
            $table->longText('detailed_col')->nullable();
            $table->longText('any_pa_or_contracted_involved')->nullable();
            $table->longText('subrogation_addressed')->nullable();
            $table->longText('tsi_logo')->nullable();
            $table->longText('miscellaneous')->nullable();
            $table->longText('report_format')->nullable();

            //  estimate
            $table->longText('price_list_current')->nullable();
            $table->longText('minimum_charges_applied')->nullable();
            $table->longText('adding_op_when_necessary')->nullable();
            $table->longText('applied_correct_deprication')->nullable();
            $table->longText('bid_items_applied_correctly')->nullable();
            $table->longText('deductible')->nullable();
            $table->longText('items_under_correct_coverage')->nullable();
            $table->longText('cabinets')->nullable();
            $table->longText('dry_wall_accurate')->nullable();
            $table->longText('debris_removal')->nullable();
            $table->longText('fencing')->nullable();
            $table->longText('flooring')->nullable();
            $table->longText('mask_and_prep')->nullable();
            $table->longText('insulation')->nullable();
            $table->longText('content_manipulation')->nullable();
            $table->longText('painting')->nullable();
            $table->longText('roof')->nullable();
            $table->longText('siding')->nullable();
            $table->longText('sketch')->nullable();
            $table->longText('tiles')->nullable();
            $table->longText('trees')->nullable();
            $table->longText('contents')->nullable();
            $table->longText('wire_shelving_proper_code')->nullable();

            //  photos
            $table->longText('photos_documenting_col')->nullable();
            $table->longText('photos_of_damaged_or_undamaged_col')->nullable();
            $table->longText('details_in_description')->nullable();
            $table->longText('elevation_photos')->nullable();
            $table->longText('address_verification')->nullable();
            $table->longText('all_slopes_of_roofs')->nullable();
            $table->longText('fence_attached')->nullable();
            $table->longText('attic')->nullable();
            $table->longText('valley_metal')->nullable();
            $table->longText('drip_edge')->nullable();
            $table->longText('shingle_gauge')->nullable();
            $table->longText('pitch_gauge')->nullable();
            $table->longText('labeled_correctly')->nullable();
            $table->longText('name_taken_by')->nullable();

            //  additional documents needed
            $table->longText('survey')->nullable();
            $table->longText('roof_form')->nullable();
            $table->longText('misc')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
