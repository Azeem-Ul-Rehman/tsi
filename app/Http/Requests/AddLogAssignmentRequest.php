<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddLogAssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'claim_number' => 'required|string',
            'tsi_file_number' => 'required|string',
            'adjuster' => 'required|integer|exists:master_adjusters,id',
            'insured' => 'nullable|string',
            'city' => 'nullable|exists:cities,id',
            'state' => 'nullable|exists:states,id',
            'zip_code' => 'nullable|string',
            'company' => 'nullable|string'
        ];
    }
}
