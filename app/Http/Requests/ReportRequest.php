<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "claim_id"                              => 'required',
            "adjuster_id"                           => 'required',
            "carrier_id"                            => "nullable",
            'category'                              => "required",

            "mortage"                               => 'nullable',
            "policy_type_and_special_provisions"    => 'nullable',
            "correct_deductable_applied"            => 'nullable',
            "time_of_inspection"                    => 'nullable',
            "date_of_inspection"                    => 'nullable',
            "name_of_who_was_inspect_at_inspection" => 'nullable',
            "detailed_investigation_summary"        => 'nullable',
            "personal_property_addressed"           => 'nullable',
            "fl_adjusters_nothing_ol_and_if_it_applies" => 'nullable',
            "detailed_col"                          => 'nullable',
            "any_pa_or_contracted_involved"         => 'nullable',
            "subrogation_addressed"                 => 'nullable',
            "tsi_logo"                              => 'nullable',
            "miscellaneous"                         => 'nullable',
            "report_format"                         => 'nullable',

            "price_list_current"                    => 'nullable',
            "minimum_charges_applied"               => 'nullable',
            "adding_op_when_necessary"              => 'nullable',
            "applied_correct_deprication"           => 'nullable',
            "bid_items_applied_correctly"           => 'nullable',
            "deductible"                            => 'nullable',
            "items_under_correct_coverage"          => 'nullable',
            "cabinets"                              => 'nullable',
            "dry_wall_accurate"                     => 'nullable',
            "debris_removal"                        => 'nullable',
            "fencing"                               => 'nullable',
            "flooring"                              => 'nullable',
            "mask_and_prep"                         => 'nullable',
            "insulation"                            => 'nullable',
            "content_manipulation"                  => 'nullable',
            "painting"                              => 'nullable',
            "roof"                                  => 'nullable',
            "siding"                                => 'nullable',
            "sketch"                                => 'nullable',
            "tiles"                                 => 'nullable',
            "trees"                                 => 'nullable',
            "contents"                              => 'nullable',
            "wire_shelving_proper_code"             => 'nullable',

            "photos_documenting_col"                => 'nullable',
            "photos_of_damaged_or_undamaged_col"    => 'nullable',
            "details_in_description"                => 'nullable',
            "elevation_photos"                      => 'nullable',
            "address_verification"                  => 'nullable',
            "all_slopes_of_roofs"                   => 'nullable',
            "fence_attached"                        => 'nullable',
            "attic"                                 => 'nullable',
            "valley_metal"                          => 'nullable',
            "drip_edge"                             => 'nullable',
            "shingle_gauge"                         => 'nullable',
            "pitch_gauge"                           => 'nullable',
            "labeled_correctly"                     => 'nullable',
            "name_taken_by"                         => 'nullable',

            "survey"                                => 'nullable',
            "roof_form"                             => 'nullable',
            "misc"                                  => 'nullable',
        ];
    }
}
