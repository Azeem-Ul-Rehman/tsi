<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMasterAdjusterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "adjuster_types.*" => "required|integer|exists:adjuster_types,id",
            "adjuster_grade" => "nullable|integer|exists:adjuster_grades,id",
            "grade_reason" => "nullable|integer|exists:grade_reasons,id",
            "certificates" => 'nullable',
            "certificates.*" => "string",
            "certificates_expiry_dates" => "nullable",
            "certificates_expiry_dates.*" => "string",
            "license_states" => "nullable",
            "license_states.*" => "integer|exists:states,id",
            "license_numbers" => "nullable",
            "license_numbers.*" => "string",
            "license_expiry_dates.*" => 'string',
            "first_name" => "required|min:3|string",
            "last_name" => "required|min:3|string",
            "email" => "required|email",
            "primary_phone_number" => "required|string",
            "secondary_phone_number" => "nullable|string",
            "emergency_contact_name" => "nullable|string",
            "emergency_contact_phone" => "nullable|string",
            "dob" => "nullable|string",
            "driver_license" => "nullable|string",
            "street_number_name" => "nullable|string",
            "city" => "nullable|exists:cities,id",
            "state" => "nullable|string",
            "zip" => "nullable|string",
            "dl_state" => "nullable|string",
            "company_llc_name" => "nullable|string",
            "company_tin" => "nullable|string",
            "tax_idds" => "nullable|string",
            "fluent_languages" => "nullable",
            "fluent_languages.*" => "string",
            "exact_address" => "nullable|string",
            "service_area_of_state" => "integer",
            "personnel_folder_made" => 'nullable|string',
            "assign_role" => "nullable|exists:assign_roles,id",
            "specialties.*" => "string",
            'inactive_rsn' => "nullable|integer|exists:personal_inactive_reasons,id",
            "initial_service_date" => "nullable|string",
            "url" => "nullable|string",
            "flagged" => "nullable|integer",
            "flag_check_date" => "nullable|string"
        ];
    }
}
