<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCorrectionRejection extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "review_date" => 'string',
            "catastrophe_claim" => 'string|required',
            "claim_number" => "string|required",
            "insd_last_name" => 'nullable|string',
            "adjuster_id" => 'integer|exists:master_adjusters,id',
            "qa" => 'nullable|exists:qas,id',
            "leader" => 'nullable|exists:leaders,id',
            "carrier" => 'nullable|exists:carriers,id',
            'fld_glr_number_exc' => 'nullable|integer',
            'fld_est_number_exc' => 'nullable|integer',
            'fld_photo_number_exc' => 'nullable|integer',
            'fld_total_number_exc' => 'nullable|integer',
            'qa_glr_number_exc' => 'nullable|integer',
            'qa_est_number_exc' => 'nullable|integer',
            'qa_photo_number_exc' => 'nullable|integer',
            'qa_total_number_exc' => 'nullable|integer',
            'glr_cov_narrative' => 'nullable|integer',
            'date_ctc_insp' => 'nullable|integer',
            'mortgage_co' => 'nullable|integer',
            'col_cause_of_loss' => 'nullable|integer',
            'subro_salv_details' => 'nullable|integer',
            'wind_hail_rpt' => 'nullable|integer',
            'new_survey' => 'nullable|integer',
            'logo_exc' => 'nullable|integer',
            'other_glr_exc' => 'nullable|integer',
            'current_xact' => 'nullable|integer',
            'deductible_application' => 'nullable|integer',
            'no_cov_dam_dele_part_all' => 'nullable|integer',
            'overhead_and_profit' => 'nullable|integer',
            'measure_accuracy' => 'nullable|integer',
            'sketch' => 'nullable|integer',
            'repair_vs_replace' => 'nullable|integer',
            'acv_vs_rcv' => 'nullable|integer',
            'depreciation' => 'nullable|integer',
            'line_item_exc' => 'nullable|integer',
            'contents_exc' => 'nullable|integer',
            'drywall' => 'nullable|integer',
            'insulation' => 'nullable|integer',
            'cleaning_mit' => 'nullable|integer',
            'paint' => 'nullable|integer',
            'trim_and_base' => 'nullable|integer',
            'flooring' => 'nullable|integer',
            'cabinets' => 'nullable|integer',
            'debris_removal' => 'nullable|integer',
            'other_est_exc' => 'nullable|integer',
            'oth_structure_proper_items' => 'nullable|integer',
            'oth_str_acv_vs_rcv' => 'nullable|integer',
            'oth_str_other_exc' => 'nullable|integer',
            'roof_wind_bp' => 'nullable|integer',
            'roof_hail_bp' => 'nullable|integer',
            'roof_valley_or_ridge' => 'nullable|integer',
            'roof_repair_vs_replace' => 'nullable|integer',
            'roof_meas_or_quantity' => 'nullable|integer',
            'roof_miss_items' => 'nullable|integer',
            'roof_add_labor' => 'nullable|integer',
            'roof_other_exc' => 'nullable|integer',
            'photo_bp' => 'nullable|integer',
            'photo_clear' => 'nullable|integer',
            'photo_all_damage' => 'nullable|integer',
            'photo_label' => 'nullable|integer',
            'exception_details' => 'nullable|string',
        ];
    }
}
