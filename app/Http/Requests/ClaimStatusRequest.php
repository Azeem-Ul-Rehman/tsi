<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClaimStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tsi_number' => 'required|string',
            'date_received' => 'nullable',
            'adjuster' => 'required|integer|exists:master_adjusters,id',
            'insured' => 'nullable|string',
            'company' => 'nullable|string',
            'claim_number' => 'nullable|string',
            'date_of_notice' => 'nullable|string',
            'date_contacted' => 'nullable|string',
            'date_inspected' => 'nullable|string',
            'report_due' => 'nullable|string',
            'note' => 'nullable|string'
        ];
    }
}
