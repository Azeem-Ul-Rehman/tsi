<?php

namespace App\Http\Controllers;

use App\Models\PersonalInactiveReason;
use Illuminate\Http\Request;

class PersonalInactiveReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reasons = PersonalInactiveReason::orderBy('reason', 'asc')->get();;

        return view('supporting-forms.personal-inactive-reasons.index', compact('reasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.personal-inactive-reasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|string'
        ]);

        PersonalInactiveReason::create([
           'name' => $request->reason
        ]);

        return redirect()->route('inactive-reasons.index')->with([
           'status' => 'success',
           'message' => 'Personal Inactive Reason created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reason = PersonalInactiveReason::findOrFail($id);

        return view('supporting-forms.personal-inactive-reasons.edit', compact('reason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'reason' => 'required|string'
        ]);
        $reason = PersonalInactiveReason::findOrFail($id);
        $reason->update([
            'name' => $request->reason
        ]);

        return back()->with([
           'status' => 'success',
           'message' => 'Personal Inactive Reason updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reason = PersonalInactiveReason::findOrFail($id);
        $reason->delete();

        return redirect()->route('inactive-reasons.index')->with([
            'status' => 'success',
            'message' => 'Personal Inactive Reason updated.'
        ]);
    }
}
