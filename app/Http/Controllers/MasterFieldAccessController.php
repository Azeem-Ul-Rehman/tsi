<?php

namespace App\Http\Controllers;

use App\Models\FieldAssignSource;
use Illuminate\Http\Request;
use App\Models\FieldAccess;

class MasterFieldAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function masterfields()
    {
        $getAllFields = FieldAccess::first();
        return view('master-adjuster.master-fields',compact('getAllFields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.field-assign-sources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = 'error';
        $getField = FieldAccess::first();
        if($request->name=="first_name"){
            $getField->first_name = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="email"){
            $getField->email = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="primary_phone_no"){
            $getField->primary_phone_no = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="city_id"){
            $getField->city_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="assign_type_id"){
            $getField->assign_type_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="state"){
            $getField->state = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="dob"){
            $getField->dob = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="initial_service_date"){
            $getField->initial_service_date = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="secondary_phone_no"){
            $getField->secondary_phone_no = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="emergency_contact_name"){
            $getField->emergency_contact_name = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="emergency_phone_no"){
            $getField->emergency_phone_no = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="street_number"){
            $getField->street_number = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="zip"){
            $getField->zip = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="driver_license"){
            $getField->driver_license = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="dl_state"){
            $getField->dl_state = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="xact_address"){
            $getField->xact_address = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="tx_id"){
            $getField->tx_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="company_tin"){
            $getField->company_tin = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="company_llc_name"){
            $getField->company_llc_name = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="state_license"){
            $getField->state_license = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="speciality"){
            $getField->speciality = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="personal_folder_made"){
            $getField->personal_folder_made = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="fluent_languages"){
            $getField->fluent_languages = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="npn_number"){
            $getField->npn_number = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="check_background_date"){
            $getField->check_background_date = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="is_flagged"){
            $getField->is_flagged = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="comment"){
            $getField->comment = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="url"){
            $getField->url = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="license_name"){
            $getField->license_name = $request->status;
            $getField->save();
            $status = 'success';
        }


        return response()->json([
            'status' => $status,
         ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $field_assign_source = FieldAssignSource::findOrFail($id);

        return view('supporting-forms.field-assign-sources.edit', compact('field_assign_source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $source = FieldAssignSource::findOrFail($id);
        $source->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Field assign source has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $source = FieldAssignSource::findOrFail($id);
        $source->delete();

        return redirect()->route('field-assign-sources.index')->with([
            'status' => 'success',
            'message' => 'Field assign source has been updated'
        ]);
    }
}
