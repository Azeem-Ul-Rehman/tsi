<?php

namespace App\Http\Controllers;

use App\Models\AssignRole;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = AssignRole::all();

        return view('supporting-forms.assign-roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.assign-roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'role' => "required|string"
        ]);

        AssignRole::create([
            'name' => $request->role
        ]);

        return redirect()->route('roles.index')->with([
            'status' => 'success',
            'message' => 'Assign Role has been created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = AssignRole::findOrFail($id);

        return view('supporting-forms.assign-roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'role' => 'required|string'
        ]);

        $role = AssignRole::findOrFail($id);
        $role->update([
           'name' => $request->role
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Assign Role has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = AssignRole::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')->with([
           'status' => 'success',
           'message' => 'Assign Role has been deleted'
        ]);
    }
}
