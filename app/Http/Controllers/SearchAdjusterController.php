<?php

namespace App\Http\Controllers;

use App\Models\AdjusterGrade;
use App\Models\AdjusterTypes;
use App\Models\AssignRole;
use App\Models\City;
use App\Models\MasterAdjuster;
use App\Models\PersonalInactiveReason;
use App\Models\StateServiceArea;
use App\Models\StateServiceAreaT1;
use Illuminate\Http\Request;

class SearchAdjusterController extends Controller
{
    public function index(){
        $states = StateServiceArea::all();
        $types = AdjusterTypes::all();
        $roles = AssignRole::all();
        $grades = AdjusterGrade::all();
        $cities = City::all();
        $inactive_reasons = PersonalInactiveReason::all();
        return view('search-adjuster.index', compact('states', 'types', 'roles', 'grades', 'cities', 'inactive_reasons'));
    }

    public function search(Request $request){
        $filter_adjusters = [];
        $adjusters = MasterAdjuster::with(['types','secondary_service_area'])->where( function($query) use ( $request ){
            if($request->has('first_name')){
                $query->where('first_name', 'like', $request->first_name.'%' );
            }

            if($request->has('last_name')){
                $query->where('last_name', 'like', $request->last_name.'%' );
            }

            if($request->grade != null){
                $query->Where('adjuster_grade_id', $request->grade);
            }

            if($request->role != null){
                $query->Where('assign_role_id', $request->role);
            }

            if($request->has('driver_license')){
                $query->Where('driver_license', $request->driver_license);
            }
            if($request->state != null){
                $query->Where('state', $request->state);
            }
            if($request->state_license != null){
                $query->whereHas('home_state_licenses',function($query) use($request){
                    $query->where('state_id',$request->state_license);
                });
            }

            if($request->inactive_reason != null){
                $query->where('inactive_reason_id', $request->inactive_reason);
            }

            if($request->city != null){
                $query->where('city_id', $request->city);
            }
        })->get();
        //return $adjusters;
        if($request->has('type') && $request->type != null){
            foreach ($adjusters as $adjuster){
                foreach ($adjuster->types as $ad_type){
                    if($ad_type->id == $request->type){
                        $filter_adjusters[] = $adjuster;
                    }
                }
            }
        }else{
           $filter_adjusters = $adjusters;
        }

        if(!empty($filter_adjusters)){

            return view('search-adjuster.search-partial', compact('filter_adjusters'));
        }else{

            return response()->json(['status' => 'fail', 'message' => "No Data Found"]);
        }
    }
}
