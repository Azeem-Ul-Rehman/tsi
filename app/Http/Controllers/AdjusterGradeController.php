<?php

namespace App\Http\Controllers;

use App\Models\AdjusterGrade;
use Illuminate\Http\Request;

class AdjusterGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adjuster_grades = AdjusterGrade::orderBy('name', 'asc')->get();

        return view('supporting-forms.adjuster-grades.index', compact('adjuster_grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.adjuster-grades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|string'
        ]);

        AdjusterGrade::create([
           'name' => $request->name
        ]);

        return redirect()->route('adjuster-grades.index')->with([
           'status' => 'success',
           'message' => 'Grade has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adjuster_grade = AdjusterGrade::findOrFail($id);

        return view('supporting-forms.adjuster-grades.edit', compact('adjuster_grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $priority = AdjusterGrade::findOrFail($id);

        $priority->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Grade has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $priority = AdjusterGrade::findOrFail($id);

        $priority->delete();

        return back()->with([
            'status' => 'success',
            'message' => 'Grade has been deleted.'
        ]);
    }
}
