<?php

namespace App\Http\Controllers;

use App\Models\AdjusterStateLicense;
use App\Models\MasterAdjuster;
use Illuminate\Http\Request;

class TabController extends Controller
{
    public function homeStates($adjuster_id){
        $adjuster = MasterAdjuster::with('home_state_licenses')
            ->where('id', $adjuster_id)
            ->first();

        return view('master-adjuster.tabs.home-states', compact('adjuster'));
    }

    public function corrections($adjuster_id){
        $adjuster = MasterAdjuster::with('corrections')
            ->where('id', $adjuster_id)
            ->first();

        return view('master-adjuster.tabs.corrections', compact('adjuster'));
    }

    public function rejections($adjuster_id){
        $adjuster = MasterAdjuster::with('rejections')
            ->where('id', $adjuster_id)
            ->first();

        return view('master-adjuster.tabs.rejections', compact('adjuster'));
    }

    public function logAssignment($adjuster_id){
        $adjuster = MasterAdjuster::with('log_assignments')
            ->where('id', $adjuster_id)
            ->first();

        return view('master-adjuster.tabs.log-assignment', compact('adjuster'));
    }

    public function claimStatus($adjuster_id){
        $adjuster = MasterAdjuster::with('claim_status')
            ->where('id', $adjuster_id)
            ->first();
        return view('master-adjuster.tabs.claim-status', compact('adjuster'));
    }
}
