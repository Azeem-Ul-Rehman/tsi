<?php

namespace App\Http\Controllers;

use App\Models\AdjusterCorrection;
use App\Models\CorrectionRejection;
use App\Models\DashboardItem;
use App\Models\MasterAdjuster;
use App\Models\State;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = [];
        $dashboard_items = Auth()->user()->dashboard_items;
        foreach ($dashboard_items as $dashboard_item) {
            if (Str::slug(strtolower($dashboard_item->name)) == 'total-adjuster') {
                $items[] = ['Total Adjusters' => MasterAdjuster::all()->count()];
            } elseif (Str::slug(strtolower($dashboard_item->name)) == 'total-field-adjuster') {
                $items[] = ['Total Field Adjusters' => MasterAdjuster::whereHas('types', function (Builder $builder) {
                    $builder->where('adjuster_type_id', 1);
                })->count()
                ];
            } elseif (Str::slug(strtolower($dashboard_item->name)) == 'total-staff-adjuster') {
                $items[] = ['Total Staff Adjusters' => MasterAdjuster::whereHas('types', function (Builder $builder) {
                    $builder->where('adjuster_type_id', 2);
                })->count()
                ];
            } elseif (Str::slug(strtolower($dashboard_item->name)) == 'out-of-office-adjuster') {
                $items[] = ['Out Of Office Adjusters' => 0];
            } elseif (Str::slug(strtolower($dashboard_item->name)) == 'suspend-or-not-use-adjuster') {
                $items[] = ['Suspended Or Not Used' => MasterAdjuster::where('active', 0)->count()];
            } elseif (Str::slug(strtolower($dashboard_item->name)) == 'adjusters-have-no-state-license') {
                $items[] = ['Adjuster Have No State License' => MasterAdjuster::doesntHave('home_state_licenses')->count()];
            } else {
                $items[] = ['Total States' => State::query()->count()];
            }
        }
        $adj_corrections = AdjusterCorrection::count();

        return view('home', compact('items','adj_corrections'));
    }
}
