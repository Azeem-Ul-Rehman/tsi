<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\ClaimStatus;
use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Http\Request;
use Session;

class AjaxController extends Controller
{
    public function getCities(Request $request){
        $cities = City::select('id', 'name')->where('state_id', $request->state_id)->get();
        if(!empty($cities)){
            return response()->json([
                'status' => 'success',
                'cities' => $cities
            ]);
        }else{
            return response()->json([
                'status' => 'fail',
                'cities' => []
            ]);
        }
    }

    public function getClaims(Request $request){
        $claims = ClaimStatus::select('id', 'claim_number')->where('master_adjuster_id', $request->adjuster_id)->get();
        if(!empty($claims)){
            return response()->json([
                'status' => 'success',
                'claims' => $claims
            ]);
        }else{
            return response()->json([
                'status' => 'fail',
                'cities' => []
            ]);
        }
    }

    public function adjustermasterslug(Request $request){
        Session::put('adjuster_master_slug',$request->adjusterslug);
        return response()->json(['status' => 'success']);
    }
}
