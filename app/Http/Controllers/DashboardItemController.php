<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DashboardItem;

class DashboardItemController extends Controller
{
    public function index(){
        $role = Auth()->user()->role->id;
        $already_have_dashboard_items = Auth()->user()->dashboard_items;
        $dashboard_items = DashboardItem::where('role_id', $role)->where('status',1)->get();

        return view('dashboard-items.index', compact('dashboard_items', 'already_have_dashboard_items'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'items' => 'required',
            'items.*' => 'exists:dashboard_items,id'
        ]);

        $user = Auth()->user()->dashboard_items()->sync($request->items);

        return redirect()->route('dashboard.item.index')->with([
            'message' => 'Dashboard Items has been updated',
            'status' => 'success'
        ]);
    }
}
