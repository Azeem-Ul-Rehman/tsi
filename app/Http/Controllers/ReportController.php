<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportRequest;
use App\Models\ClaimStatus;
use App\Models\MasterAdjuster;
use App\Models\Report;
use Illuminate\Database\Eloquent\Builder;

class ReportController extends Controller
{
    public function index()
    {
        $reports = Report::all();

        return view('report.index', compact('reports'));
    }

    public function create()
    {
        $adjusters = MasterAdjuster::whereHas('types', function (Builder $query) {
            $query->where('adjuster_type_id', 1);
        })->get();

        return view('report.create', compact('adjusters'));
    }

    public function store(ReportRequest $request)
    {

        $checkRecord = Report::where([
            'adjuster_id' => $request->adjuster_id,
            'claim_id' => $request->claim_id,
            'category' => $request->category,
        ])->first();
        if (!is_null($checkRecord)) {
            return redirect()->back()->with([
                'status' => 'error',
                'message' => 'Record Already Exist for this adjuster,claim and category.'
            ]);
        }

        if ($request->category == 'report') {
            Report::create([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'mortage' => $request->mortage,
                'policy_type_and_special_provisions' => $request->policy_type_and_special_provisions,
                'correct_deductable_applied' => $request->correct_deductable_applied,
                'time_of_inspection' => $request->time_of_inspection,
                'date_of_inspection' => $request->date_of_inspection,
                'name_of_who_was_inspect_at_inspection' => $request->name_of_who_was_inspect_at_inspection,
                'detailed_investigation_summary' => $request->detailed_investigation_summary,
                'personal_property_addressed' => $request->personal_property_addressed,
                'fl_adjusters_nothing_ol_and_if_it_applies' => $request->fl_adjusters_nothing_ol_and_if_it_applies,
                'detailed_col' => $request->detailed_col,
                'any_pa_or_contracted_involved' => $request->any_pa_or_contracted_involved,
                'subrogation_addressed' => $request->subrogation_addressed,
                'tsi_logo' => $request->tsi_logo,
                'miscellaneous' => $request->miscellaneous,
                'report_format' => $request->report_format,
            ]);
        } elseif ($request->category == 'estimate') {
            Report::create([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'price_list_current' => $request->price_list_current,
                'minimum_charges_applied' => $request->minimum_charges_applied,
                'adding_op_when_necessary' => $request->adding_op_when_necessary,
                'applied_correct_deprication' => $request->applied_correct_deprication,
                'bid_items_applied_correctly' => $request->bid_items_applied_correctly,
                'deductible' => $request->deductible,
                'items_under_correct_coverage' => $request->items_under_correct_coverage,
                'cabinets' => $request->cabinets,
                'dry_wall_accurate' => $request->dry_wall_accurate,
                'debris_removal' => $request->debris_removal,
                'fencing' => $request->fencing,
                'flooring' => $request->flooring,
                'mask_and_prep' => $request->mask_and_prep,
                'insulation' => $request->insulation,
                'content_manipulation' => $request->content_manipulation,
                'painting' => $request->painting,
                'roof' => $request->roof,
                'siding' => $request->siding,
                'sketch' => $request->sketch,
                'tiles' => $request->tiles,
                'trees' => $request->trees,
                'contents' => $request->contents,
                'wire_shelving_proper_code' => $request->wire_shelving_proper_code,
            ]);
        } elseif ($request->category == 'photos') {
            Report::create([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'photos_documenting_col' => $request->photos_documenting_col,
                'photos_of_damaged_or_undamaged_col' => $request->photos_of_damaged_or_undamaged_col,
                'details_in_description' => $request->details_in_description,
                'elevation_photos' => $request->elevation_photos,
                'address_verification' => $request->address_verification,
                'all_slopes_of_roofs' => $request->all_slopes_of_roofs,
                'fence_attached' => $request->fence_attached,
                'attic' => $request->attic,
                'valley_metal' => $request->valley_metal,
                'drip_edge' => $request->drip_edge,
                'shingle_gauge' => $request->shingle_gauge,
                'pitch_gauge' => $request->pitch_gauge,
                'labeled_correctly' => $request->labeled_correctly,
                'name_taken_by' => $request->name_taken_by,
            ]);
        } elseif ($request->category == 'additional_documents') {
            Report::create([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'survey' => $request->survey,
                'roof_form' => $request->roof_form,
                'misc' => $request->misc,
            ]);
        }


        return redirect()->route('report.index')->with([
            'status' => 'success',
            'message' => 'Report has been added.'
        ]);
    }

    public function show($id)
    {
        $report = Report::findOrFail($id);

        return view('report.show', compact('report'));
    }

    public function edit($id)
    {
        $claims = ClaimStatus::all();
        $adjusters = MasterAdjuster::whereHas('types', function (Builder $query) {
            $query->where('adjuster_type_id', 1);
        })->get();
        $report = Report::findOrFail($id);


        return view('report.edit', compact('claims', 'adjusters', 'report'));
    }

    public function update(ReportRequest $request, $id)
    {
        $report = Report::findOrFail($id);
        if ($request->category == 'report') {
            $report->update([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'mortage' => $request->mortage,
                'policy_type_and_special_provisions' => $request->policy_type_and_special_provisions,
                'correct_deductable_applied' => $request->correct_deductable_applied,
                'time_of_inspection' => $request->time_of_inspection,
                'date_of_inspection' => $request->date_of_inspection,
                'name_of_who_was_inspect_at_inspection' => $request->name_of_who_was_inspect_at_inspection,
                'detailed_investigation_summary' => $request->detailed_investigation_summary,
                'personal_property_addressed' => $request->personal_property_addressed,
                'fl_adjusters_nothing_ol_and_if_it_applies' => $request->fl_adjusters_nothing_ol_and_if_it_applies,
                'detailed_col' => $request->detailed_col,
                'any_pa_or_contracted_involved' => $request->any_pa_or_contracted_involved,
                'subrogation_addressed' => $request->subrogation_addressed,
                'tsi_logo' => $request->tsi_logo,
                'miscellaneous' => $request->miscellaneous,
                'report_format' => $request->report_format,
            ]);
        } elseif ($request->category == 'estimate') {
            $report->update([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'price_list_current' => $request->price_list_current,
                'minimum_charges_applied' => $request->minimum_charges_applied,
                'adding_op_when_necessary' => $request->adding_op_when_necessary,
                'applied_correct_deprication' => $request->applied_correct_deprication,
                'bid_items_applied_correctly' => $request->bid_items_applied_correctly,
                'deductible' => $request->deductible,
                'items_under_correct_coverage' => $request->items_under_correct_coverage,
                'cabinets' => $request->cabinets,
                'dry_wall_accurate' => $request->dry_wall_accurate,
                'debris_removal' => $request->debris_removal,
                'fencing' => $request->fencing,
                'flooring' => $request->flooring,
                'mask_and_prep' => $request->mask_and_prep,
                'insulation' => $request->insulation,
                'content_manipulation' => $request->content_manipulation,
                'painting' => $request->painting,
                'roof' => $request->roof,
                'siding' => $request->siding,
                'sketch' => $request->sketch,
                'tiles' => $request->tiles,
                'trees' => $request->trees,
                'contents' => $request->contents,
                'wire_shelving_proper_code' => $request->wire_shelving_proper_code,
            ]);
        } elseif ($request->category == 'photos') {
            $report->update([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'photos_documenting_col' => $request->photos_documenting_col,
                'photos_of_damaged_or_undamaged_col' => $request->photos_of_damaged_or_undamaged_col,
                'details_in_description' => $request->details_in_description,
                'elevation_photos' => $request->elevation_photos,
                'address_verification' => $request->address_verification,
                'all_slopes_of_roofs' => $request->all_slopes_of_roofs,
                'fence_attached' => $request->fence_attached,
                'attic' => $request->attic,
                'valley_metal' => $request->valley_metal,
                'drip_edge' => $request->drip_edge,
                'shingle_gauge' => $request->shingle_gauge,
                'pitch_gauge' => $request->pitch_gauge,
                'labeled_correctly' => $request->labeled_correctly,
                'name_taken_by' => $request->name_taken_by,
            ]);
        } elseif ($request->category == 'additional_documents') {
            $report->update([
                'claim_id' => $request->claim_id,
                'adjuster_id' => $request->adjuster_id,
                'carrier_id' => $request->carrier_id,
                'category' => $request->category,
                'survey' => $request->survey,
                'roof_form' => $request->roof_form,
                'misc' => $request->misc,
            ]);
        }

        return redirect()->back()->with([
            'status' => 'success',
            'message' => 'Report has been updated.'
        ]);
    }

    public function destroy($id)
    {
        $report = Report::findOrFail($id);
        $report->delete();

        return redirect()->route('report.index')->with([
            'status' => 'success',
            'message' => 'Report has been deleted.'
        ]);
    }
}
