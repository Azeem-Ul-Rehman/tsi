<?php

namespace App\Http\Controllers;

use App\Models\ClAssignType;
use Illuminate\Http\Request;

class ClAssignTypeController extends Controller
{
    public function index(){
        $cl_assign_types = ClAssignType::orderBy('name', 'asc')->get();

        return view('supporting-forms.cl-assign-types.index', compact('cl_assign_types'));
    }

    public function create(){
        return view('supporting-forms.cl-assign-types.create');
    }

    public function store(Request $request){
        $this->validate($request, [
           'name' => 'required|string'
        ]);

        ClAssignType::create([
           'name' => $request->name
        ]);

        return redirect()->route('cl-assign-types.index')->with([
            'status' => 'success',
            'message' => 'Cl Assign Type has been added.'
        ]);
    }

    public function edit($id){
        $cl_assign_type = ClAssignType::findOrFail($id);

        return view('supporting-forms.cl-assign-types.edit', compact('cl_assign_type'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        $res = ClAssignType::findOrFail($id);
        $res->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Cl Assign Type has been added.'
        ]);
    }

    public function destroy($id){
        $cl_assign_type = ClAssignType::findOrFail($id);
        $cl_assign_type->delete();

        return redirect()->route('cl-assign-types.index')->with([
            'status' => 'success',
            'message' => 'Cl Assign Type has been deleted.'
        ]);
    }
}
