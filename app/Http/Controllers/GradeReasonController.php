<?php

namespace App\Http\Controllers;

use App\Models\GradeReason;
use Illuminate\Http\Request;

class GradeReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grade_reasons = GradeReason::orderBy('name', 'asc')->get();;

        return view('supporting-forms.grade-reasons.index', compact('grade_reasons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.grade-reasons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        GradeReason::create([
            'name' => $request->name
        ]);

        return redirect()->route('grade-reasons.index')->with([
            'status' => 'success',
            'message' => 'Grade Reason has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade_reason = GradeReason::findOrFail($id);

        return view('supporting-forms.grade-reasons.edit', compact('grade_reason'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $priority = GradeReason::findOrFail($id);

        $priority->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Grade Reason has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $priority = GradeReason::findOrFail($id);

        $priority->delete();

        return back()->with([
            'status' => 'success',
            'message' => 'Grade Reason has been deleted.'
        ]);
    }
}
