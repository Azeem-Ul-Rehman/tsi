<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LogFieldAccess;

class LogFieldAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logfields()
    {
        $getAllFields = LogFieldAccess::first();
        return view('log-assignment.log-fields',compact('getAllFields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logstore(Request $request)
    {
        $status = 'error';
        $getField = LogFieldAccess::first();
        if($request->name=="claim_number"){
            $getField->claim_number = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="master_adjuster_id"){
            $getField->master_adjuster_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="tsi_file_number"){
            $getField->tsi_file_number = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="city_id"){
            $getField->city_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="state_id"){
            $getField->state_id = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="zip_code"){
            $getField->zip_code = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="date"){
            $getField->date = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="company"){
            $getField->company = $request->status;
            $getField->save();
            $status = 'success';
        }
        else if($request->name=="status"){
            $getField->status = $request->status;
            $getField->save();
            $status = 'success';
        }

        return response()->json([
            'status' => $status,
         ]);
    }

}
