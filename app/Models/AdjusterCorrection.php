<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class AdjusterCorrection extends Model
{
    protected $table = 'adjuster_corrections';
    protected $guarded = [];

    public function field_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'master_adjuster_id', 'id');
    }

    public function carrier(){
        return $this->belongsTo(Carrier::class, 'carrier_id', 'id');
    }

    public function leader(){
        return $this->belongsTo(Leader::class, 'leader_id', 'id');
    }

    public function qa(){
        return $this->belongsTo(Qa::class, 'qa_id', 'id');
    }
    public function setReviewDateAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);

        return $this->attributes['review_date'] = $datetime->format('Y-m-d');;
    }

    public function getReviewDateAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
