<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $guarded = [];

    public function claim()
    {
        return $this->belongsTo(ClaimStatus::class, 'claim_id', 'id');
    }
    public function master_adjuster()
    {
        return $this->belongsTo(MasterAdjuster::class, 'adjuster_id', 'id');
    }
}
