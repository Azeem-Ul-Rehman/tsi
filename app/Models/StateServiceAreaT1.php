<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateServiceAreaT1 extends Model
{
    protected $table = 'state_service_areas_t1';
    protected $guarded = [];

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'state_service_area_t1_id', 'id');
    }
}
