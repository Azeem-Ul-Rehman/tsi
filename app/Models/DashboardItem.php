<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role;

class DashboardItem extends Model
{
    protected $table = 'dashboard_items';
    protected $guarded = [];
    
    public function role(){
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}
