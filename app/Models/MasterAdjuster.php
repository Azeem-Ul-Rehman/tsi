<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class MasterAdjuster extends Model
{
    protected $table = 'master_adjusters';
    protected $guarded = [];

   // protected $dates = ['check_background_date', 'initial_service_date'];

    public function types(){
        return $this->belongsToMany(AdjusterTypes::class,'adjuster_type_master_adjuster', 'master_adjuster_id', 'adjuster_type_id');
    }

    public function type_field(){
        return $this->belongsToMany(AdjusterTypes::class,'adjuster_type_master_adjuster', 'master_adjuster_id', 'adjuster_type_id')
            ->where('name', 'Field');
    }

    public function type_desk(){
        return $this->belongsToMany(AdjusterTypes::class,'adjuster_type_master_adjuster', 'master_adjuster_id', 'adjuster_type_id')
            ->where('name', 'Desk');
    }

    public function certificates(){
        return $this->hasMany(AdjusterCertificate::class, 'master_adjuster_id','id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function state(){
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function home_state_licenses(){
        return $this->hasMany(AdjusterStateLicense::class, 'master_adjuster_id', 'id');
    }

    public function grade(){
        return $this->belongsTo(AdjusterGrade::class, 'adjuster_grade_id', 'id');
    }

    public function grade_reason(){
        return $this->belongsTo(GradeReason::class, 'grade_reason_id', 'id');
    }

    public function assign_role(){
        return $this->belongsTo(AssignRole::class, 'assign_role_id', 'id');
    }

    public function inactive_reason(){
        return $this->belongsTo(PersonalInactiveReason::class, 'personal_inactive_reason_id', 'id');
    }

    public function corrections(){
        return $this->hasMany(AdjusterCorrection::class, 'master_adjuster_id', 'id');
    }

    public function rejections(){
        return $this->hasMany(CorrectionRejection::class, 'adjuster_id', 'id');
    }

    public function log_assignments(){
        return $this->hasMany(LogAssignment::class, 'master_adjuster_id', 'id');
    }

    public function claim_status(){
        return $this->hasMany(ClaimStatus::class, 'master_adjuster_id', 'id');
    }

    public function getFullName(){
        return $this->first_name.' '.$this->last_name;
    }

    public function setURLAttribute($value){
        return $this->attributes['url'] = 'www.'. $value;
    }

    public function getURL(){
        return substr($this->url, 4);
    }

    public function service_state_area(){
        return $this->belongsTo(StateServiceAreaT1::class, 'state_server_area_t1_id', 'id');
    }

    public function scopeIdDescending($query){
        return $query->orderBy('id', 'DESC');
    }

    public function setInitialServiceDateAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['initial_service_date'] = $datetime == null ? null: $datetime->format('Y-m-d');
    }

    public function getInitialServiceDateAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }

    public function setDobAttribute($value){
        $datetime = $value == null ? null :  DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['dob'] = $datetime== null ? null: $datetime->format('Y-m-d');
    }

    public function getDobAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }

    public function setFlagCheckDateAttribute($value){
        $datetime = $value == null ? null :  DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['flag_check_date'] = $datetime== null ? null: $datetime->format('Y-m-d');
    }

    public function getFlagCheckDateAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }

    public function setInactiveEndDateAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['inactive_end_date'] = $datetime ==null ? null : $datetime->format('Y-m-d');
    }

    public function getInactiveEndDateAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }

    public function setInactiveStartDateAttribute($value){
        $datetime = $value ==null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['inactive_start_date'] = $datetime==null ? null : $datetime->format('Y-m-d');
    }

    public function getInactiveStartDateAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }

    public function setCheckBackgroundDateAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['check_background_date'] = $datetime==null ? null : $datetime->format('Y-m-d');
    }

    public function getCheckBackgroundDateAttribute($value){
        return $value == null ? null : Carbon::parse($value)->format('m-d-Y');
    }
}
