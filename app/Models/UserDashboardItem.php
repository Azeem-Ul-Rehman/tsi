<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDashboardItem extends Model
{
    protected $table = 'dashboard_item_user';
    protected $guarded = [];

}
