<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalStatus extends Model
{
    protected $guarded = [];
    protected $table = 'personal_status';
}
