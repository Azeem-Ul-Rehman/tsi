<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GradeReason extends Model
{
    protected $table = 'grade_reasons';
    protected $guarded = [];
}
