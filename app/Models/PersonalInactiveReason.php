<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalInactiveReason extends Model
{
    protected $table = 'personal_inactive_reasons';
    protected $guarded = [];

    public function masterAdjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'personal_inactive_reason_id', 'id');
    }
}
