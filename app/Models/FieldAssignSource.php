<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldAssignSource extends Model
{
    protected $table = 'field_assign_sources';
    protected $guarded = [];
}
